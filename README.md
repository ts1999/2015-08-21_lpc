# staRt mobile app #

### Overviews ###

* [Project website](http://wp.nyu.edu/byunlab/projects/start/)
* [App architecture (slide stack)](https://docs.google.com/presentation/d/1SMhbelWZ9-PcoNv52PiFhXDP-O7snmqbzbqecV_8C1M/edit)
* [Release schedule](https://workflowy.com/s/M5KVSRhWJ0#)

### Documentation ###

*Please add documentation in the form of hyperlinks to Google docs. Include a descriptive title indicating what aspect of the program you are documenting.*

* [Documentation of C++ code (Jon Forsyth)](https://drive.google.com/file/d/0B5D9Tl6d0WzleURVNkVJNmdhMjA/view?usp=sharing) 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines