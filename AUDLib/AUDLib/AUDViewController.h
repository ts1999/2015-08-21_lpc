//
//  AUDViewController.h
//  AUDLib
//
//  Created by Tim Bolstad on 12/5/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AUDGraphController;

@interface AUDViewController : UIViewController

@property (strong, nonatomic) AUDGraphController *audioController;
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;

- (IBAction)volumeSlider:(UISlider *)sender;


@end
