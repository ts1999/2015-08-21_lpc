1. Copy AudLib project folder to same folder that contains your project.
2. Drag AudLib.xcodeproj file into to your project’s Project Navigator.
3. Under Project/BuildSettings, add a header search path
	- relative search path if AudLib project is in the same folder as your project.
	“../AUDLib/AUDLib/Source”
4. Under Targets / Build Phases 
	- link binary to libAUDLib.a
	- add Target Dependency to AUDLib/Static Library so that it automatically builds.
