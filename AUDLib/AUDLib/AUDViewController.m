//
//  AUDViewController.m
//  AUDLib
//
//  Created by Tim Bolstad on 12/5/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import "AUDViewController.h"
#import "AUDGraphController.h"

@interface AUDViewController ()

@end

@implementation AUDViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)volumeSlider:(UISlider *)sender {
    
    CGFloat val = sender.value;
    [self.audioController setVolume:val
                            atIndex:0];
}
@end
