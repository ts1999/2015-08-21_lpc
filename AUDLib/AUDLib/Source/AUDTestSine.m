//
//  AUDTestSine.m
//  AUDLib
//
//  Created by Tim Bolstad on 12/7/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import "AUDTestSine.h"
#import "AUDGraphController.h"

@implementation AUDTestSine

- (id)init
{
    if (self = [super init]) {
        _frequency = 600.0;
        sinPhase = 0.0;
        _sampleRate = 41000.0;
    }
    return self;
}

- (void)addToGraph:(AUDGraphController*)graphController
      AtMixerIndex:(NSInteger)mixerIndex
{
    self.sampleRate = graphController.sampleRate;
    // Test Sinewave
    AURenderCallbackStruct callbackStruct = [self audioCallback];
    [graphController connectAudioCallback:callbackStruct
                          toMixerInputBus:0];
    [graphController updateGraph];
}

static OSStatus sinewaveInput(void *inRefCon,
                              AudioUnitRenderActionFlags *ioActionFlags,
                              const AudioTimeStamp *inTimeStamp,
                              UInt32 inBusNumber,
                              UInt32 inNumberFrames,
                              AudioBufferList *ioData)
{
	// Get a reference to the object that was passed with the callback
	AUDTestSine *THIS = (__bridge AUDTestSine*)inRefCon;
    Float64 sampleRate = THIS->_sampleRate;
    Float32 freq = THIS->_frequency;
	// Get a pointer to the dataBuffer of the AudioBufferList
    Float32 *outA = (Float32 *)ioData->mBuffers[0].mData;
	//Float32 *outB = (Float32 *)ioData->mBuffers[1].mData;
    
	// The amount the phase changes in  single sample
	double phaseIncrement = 2 * M_PI * freq / sampleRate;
	// Pass in a reference to the phase value, you have to keep track of this
	// so that the sin resumes right where the last call left off
	Float32 phase = THIS->sinPhase;
    
	double sinSignal;
	// Loop through the callback buffer, generating samples
	for (UInt32 i = 0; i < inNumberFrames; ++i) {
        
        // calculate the next sample
        sinSignal = sin(phase);
        outA[i] = sinSignal;
        //outB[i] = sinSignal;
        phase = phase + phaseIncrement;
    }
    // Reset the phase value to prevent the float from overflowing
    if (phase >= 2 * M_PI * freq) {
		phase = phase - 2 * M_PI * freq;
	}
	// Store the phase for the next callback.
	THIS->sinPhase = phase;
    
	return noErr;
}

// Bundle up the callback and the reference pointer
- (AURenderCallbackStruct)audioCallback
{
    // Callback Struct
	AURenderCallbackStruct renderCallbackStruct;
    // Function Pointer
	renderCallbackStruct.inputProc = &sinewaveInput;
	// Object Reference
    renderCallbackStruct.inputProcRefCon = (__bridge void*)self;
	return renderCallbackStruct;
}

@end
