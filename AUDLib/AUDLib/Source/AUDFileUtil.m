//
//  AUDFileUtil.m
//  AUDLib
//
//  Created by Tim Bolstad on 12/26/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import "AUDFileUtil.h"

@implementation AUDFileUtil

+ (void)closeAudioFile:(ExtAudioFileRef)extAudioFile
             DidModify:(BOOL)didModify
{
    if (didModify) {
        // Get AudioFileID
        AudioFileID audioFileID =
        [AUDFileUtil fileIDForExtAudioFile:extAudioFile];
        // Optimize close and dispose.
        AudioFileOptimize(audioFileID);
        AudioFileClose(audioFileID);
    }
    ExtAudioFileDispose(extAudioFile);
}

+ (AudioFileID)fileIDForExtAudioFile:(ExtAudioFileRef)fileRef
{
    AudioFileID audioFileID;
    UInt32 dataSize = sizeof(AudioFileID);
    ExtAudioFileGetProperty(fileRef,
                            kExtAudioFileProperty_AudioFile,
                            &dataSize,
                            &audioFileID);
    return audioFileID;
}

+ (ExtAudioFileRef)createNewFile:(NSString*)filePath
                        FileASBD:(AudioStreamBasicDescription)fileASBD
                  WithStreamASBD:(AudioStreamBasicDescription)streamASBD
{
    OSStatus result;
	AudioFileTypeID afTypeID = kAudioFileWAVEType;
	AudioFileID	audioFileID;
    ExtAudioFileRef extDestFile;
	CFURLRef fileURL;
    fileURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
                                            (CFStringRef)filePath,
                                            kCFURLPOSIXPathStyle,
                                            false);
	// Create the destination audio file
	result = AudioFileCreateWithURL(fileURL,
									afTypeID,
									&fileASBD,
									kAudioFileFlags_EraseFile,
									&audioFileID);
	if (result) NSLog(@"Couldn't Create File: %d", (int)result);
	// Wrap file extAudioFileObject
	result = ExtAudioFileWrapAudioFileID(audioFileID, true, &extDestFile);
	if (result) NSLog(@"Couldn't Wrap File: %d", (int)result);
	// Set file stream description
	result = ExtAudioFileSetProperty(extDestFile,
									 kExtAudioFileProperty_ClientDataFormat,
									 sizeof(AudioStreamBasicDescription),
									 &streamASBD);
	if (result) NSLog(@"Couldn't Set File Stream Format: %d %u", (int)result, (unsigned int)streamASBD.mChannelsPerFrame );
	// pre-write nil to prevent allocation pauses in the render callback
    result = ExtAudioFileWriteAsync(extDestFile, 0, NULL);
	if (result) NSLog(@"Couldn't Pre Write File: %d", (int)result);
    
    return extDestFile;
}

+ (OSStatus)openAudioFile:(NSString*)filePath
               OutFileRef:(ExtAudioFileRef*)fileRef
                  OutDesc:(AudioStreamBasicDescription*)asbd
{
    // create the URL
	CFURLRef sourceURL;
    sourceURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
                                              (CFStringRef)filePath,
                                              kCFURLPOSIXPathStyle,
                                              false);
	// Open source file
	OSStatus result = ExtAudioFileOpenURL(sourceURL, fileRef);
	if (result || !fileRef) {
        printf("ExtAudioFileOpenURL result %d %08X %4.4s\n",
               (int)result, (int)result, (char*)&result);
        return result;
    }
    
    // Clear Description
	memset(asbd, 0, sizeof(AudioStreamBasicDescription));
    // Get file's audio format description
	UInt32 propSize = sizeof(*asbd);
	result = ExtAudioFileGetProperty(*fileRef,
                                     kExtAudioFileProperty_FileDataFormat,
                                     &propSize,
                                     asbd);
	if (result) {
        printf("Get File Format Description: %d %08X %4.4s\n",
               (int)result, (int)result, (char*)&result); return result;
    }
    // Cleanup URL
    CFRelease(sourceURL);
    
    return result;
}

+ (SInt64)fileFrameLength:(ExtAudioFileRef)fileRef
{
	// get the file's length in sample frames
    OSStatus result;
	SInt64 numFrames = 0;
	UInt32 propSize = sizeof(numFrames);
	result = ExtAudioFileGetProperty(fileRef,
                                     kExtAudioFileProperty_FileLengthFrames,
                                     &propSize,
                                     &numFrames);
	if (result) {
        printf("Get Ext FileLengthFrames %d %08X %4.4s\n",
               (int)result, (int)result, (char*)&result);
    }
    return numFrames;
}

@end
