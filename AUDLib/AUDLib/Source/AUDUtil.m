//
//  AUDUtility.m
//  AUDLib
//
//  Created by Tim Bolstad on 12/8/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import "AUDUtil.h"

void CAError(OSStatus error, const char *operation)
{
    if (error == noErr) return;
    char errorString[20];
    memset(errorString, 0, 20*sizeof(char));
    // See if it appears to be a 4-char-code
    *(UInt32 *)(errorString + 1) = CFSwapInt32HostToBig(error);
    if (isprint(errorString[1]) && isprint(errorString[2]) &&
        isprint(errorString[3]) && isprint(errorString[4]))
    {
        errorString[0] = errorString[5] = '\'';
        errorString[6] = '\0';
    } else
        // No, format it as an integer
        sprintf(errorString, "%d", (int)error);
    fprintf(stderr, "Error: %s (%s)\n", operation, errorString);
    exit(1);
}

@implementation AUDUtil

#pragma mark AudioBufferLists
+ (AudioBufferList*) bufferListWithChannels:(UInt32)numChannels
												 andSize:(UInt32)size
										 allocateBuffers:(BOOL)allocate
{
	AudioBufferList *audioBufferList;
	UInt32	i;
	
	audioBufferList = (AudioBufferList*)calloc(1, sizeof(AudioBufferList) + numChannels * sizeof(AudioBuffer));
	if(audioBufferList == NULL)
		return NULL;
	
	audioBufferList->mNumberBuffers = numChannels;
	for(i = 0; i < numChannels; ++i) {
		audioBufferList->mBuffers[i].mNumberChannels = 1;
		audioBufferList->mBuffers[i].mDataByteSize = size;
        if (allocate) {
            audioBufferList->mBuffers[i].mData = malloc(size);
            if(audioBufferList->mBuffers[i].mData == NULL) {
                [AUDUtil destroyBufferList:audioBufferList];
                return NULL;
            }
        }
	}
	return audioBufferList;
}

// Convenience function to dispose of our audio buffers
+ (void) destroyBufferList:(AudioBufferList*)list
{
	UInt32	i;
	if(list) {
		for(i = 0; i < list->mNumberBuffers; i++) {
			if(list->mBuffers[i].mData){
				free(list->mBuffers[i].mData);
			}
		}
		free(list);
	}
}

#pragma mark - Stream Formats
+ (void) streamFormatMonoFloat:(AudioStreamBasicDescription*)asbd
                    SampleRate:(Float64)sampleRate
{
    // 32-bit float -> 4 bytes
    const int sampleSize = sizeof(Float32);
    asbd->mFormatID = kAudioFormatLinearPCM;
    asbd->mFormatFlags =    kAudioFormatFlagIsFloat |
                            kAudioFormatFlagIsPacked |
                            kAudioFormatFlagIsNonInterleaved;
    asbd->mFramesPerPacket = 1;
    asbd->mBytesPerFrame = sampleSize;
    asbd->mBytesPerPacket = asbd->mBytesPerFrame * asbd->mFramesPerPacket;
    asbd->mChannelsPerFrame = 1;
    asbd->mBitsPerChannel = sampleSize * 8; // 8-bits per byte
    asbd->mSampleRate = sampleRate;
}

+ (void) streamFormatStereoFloat:(AudioStreamBasicDescription*)asbd
                      SampleRate:(Float64)sampleRate
{
    // 32-bit float -> 4 bytes
    const int sampleSize = sizeof(Float32);
    asbd->mFormatID = kAudioFormatLinearPCM;
    asbd->mFormatFlags =    kAudioFormatFlagIsFloat |
                            kAudioFormatFlagIsPacked |
                            kAudioFormatFlagIsNonInterleaved;
    asbd->mFramesPerPacket = 1;
    asbd->mBytesPerFrame = sampleSize;
    asbd->mBytesPerPacket = asbd->mBytesPerFrame * asbd->mFramesPerPacket;
    asbd->mChannelsPerFrame = 2;
    asbd->mBitsPerChannel = sampleSize * 8; // 8-bits per byte
    asbd->mSampleRate = sampleRate;
}
    
+ (void) streamFormatMonoInt:(AudioStreamBasicDescription*)asbd
                  SampleRate:(Float64)sampleRate
{
    asbd->mSampleRate = sampleRate;
    asbd->mChannelsPerFrame = 1;
    asbd->mFormatID = kAudioFormatLinearPCM;
    asbd->mFormatFlags = 
                        kLinearPCMFormatFlagIsPacked |
                         kLinearPCMFormatFlagIsSignedInteger; // little-endian
    asbd->mBitsPerChannel = 16;
    asbd->mBytesPerPacket = asbd->mBytesPerFrame = 2 * asbd->mChannelsPerFrame;
    asbd->mFramesPerPacket = 1;
    
//    // 32-bit float -> 4 bytes
//    const int sampleSize = sizeof(AudioSampleType);
//    asbd->mFormatID = kAudioFormatLinearPCM;
//    asbd->mFormatFlags = kAudioFormatFlagIsSignedInteger |
//                         kAudioFormatFlagIsPacked;
//    asbd->mFramesPerPacket = 1;
//    asbd->mBytesPerFrame = sampleSize;
//    asbd->mBytesPerPacket = asbd->mBytesPerFrame * asbd->mFramesPerPacket;
//    asbd->mChannelsPerFrame = 2;
//    asbd->mBitsPerChannel = sampleSize * 8; // 8-bits per byte
//    asbd->mSampleRate = sampleRate;
}

// ASBD Troubleshooting Method from Apple
+ (void)printASBD:(AudioStreamBasicDescription)asbd
{
    char formatIDString[5];
    UInt32 formatID = CFSwapInt32HostToBig (asbd.mFormatID);
    bcopy (&formatID, formatIDString, 4);
    formatIDString[4] = '\0';
    
    NSLog (@"  Sample Rate:        %10.0f", asbd.mSampleRate);
    NSLog (@"  Format ID:          %10s", formatIDString);
    NSLog (@"  Format Flags:       %10u", (unsigned int)asbd.mFormatFlags);
    NSLog (@"  Bytes per Packet:   %10u", (unsigned int)asbd.mBytesPerPacket);
    NSLog (@"  Frames per Packet:  %10u", (unsigned int)asbd.mFramesPerPacket);
    NSLog (@"  Bytes per Frame:    %10u", (unsigned int)asbd.mBytesPerFrame);
    NSLog (@"  Channels per Frame: %10u", (unsigned int)asbd.mChannelsPerFrame);
    NSLog (@"  Bits per Channel:   %10u", (unsigned int)asbd.mBitsPerChannel);
}

+ (void)printTestBuffer:(AudioBufferList*)ioData
{
    AudioBuffer *buffArr = ioData->mBuffers;
    AudioBuffer buff = buffArr[0];
    SInt16 *data = buff.mData;
    for (int i = 0; i < 3; i++) {
        NSLog(@"Out: %dh %i", data[i], i);
    }
}

+ (void)convertIntBuffer:(SInt16**)inBuffer
           ToFloatBuffer:(Float32**)outBuffer
                    Size:(UInt64*)outSize
{
    int bufferSize = (int)(*outSize * sizeof(Float32));
    Float32 *floatBuf = malloc(bufferSize);
    SInt16  *intBuf   = *inBuffer;
    
    for (int i = 0; i < *outSize; ++i) {
        
        SInt16 val = intBuf[i];
        floatBuf[i] = (Float32) (val / 32767.0f);
    }
    *outBuffer = floatBuf;
    free(*inBuffer);
}

@end
