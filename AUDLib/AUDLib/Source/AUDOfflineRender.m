//
//  AUDOfflineRender.m
//  AUDLib
//
//  Created by Tim Bolstad on 12/23/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//
#import "AUDOfflineRender.h"
#import "AUDUtil.h"
#import "AUDFileUtil.h"

@interface AUDOfflineRender ()
{
    AudioBuffer *sampleBuffer;
    ExtAudioFileRef inputFile;
    ExtAudioFileRef destinationFile;
}
@end

@implementation AUDOfflineRender

- (void)processInputFile:(NSString*)sourceURLString
              OutputFile:(NSString*)destinationURLString
{
    // Stream Formats
    AudioStreamBasicDescription inFileASBD;
    AudioStreamBasicDescription outFileASBD;
    [AUDUtil streamFormatMonoInt:&outFileASBD
                      SampleRate:44100];
    AudioStreamBasicDescription workASBD;
    [AUDUtil streamFormatMonoFloat:&workASBD
                      SampleRate:44100];
    
    // Open File
    [AUDFileUtil openAudioFile:sourceURLString
                    OutFileRef:&inputFile
                       OutDesc:&inFileASBD];
    // Get File Length
    SInt64 frames = [AUDFileUtil fileFrameLength:inputFile];
    NSLog(@"Frames: %lli", frames);
    
    // Set Client stream format
    OSStatus result;
	UInt32 propSize = sizeof(workASBD);
	result = ExtAudioFileSetProperty(inputFile,
                                     kExtAudioFileProperty_ClientDataFormat,
                                     propSize,
                                     &workASBD);
    // Setup Work Buffer
    AudioBufferList *workBuffer;
    UInt32 workingFrames = 4096;
    UInt32 sampleSize = workASBD.mBytesPerFrame; //sizeof(SInt16);
    UInt32 workBufferSize = (UInt32)(workingFrames * sampleSize);
    workBuffer = [AUDUtil bufferListWithChannels:1
                                         andSize:workBufferSize
                                 allocateBuffers:YES];
    // Create output file
    destinationFile = [AUDFileUtil createNewFile:destinationURLString
                                        FileASBD:outFileASBD
                                  WithStreamASBD:workASBD];
    
    // ***** Process *****
    SInt64 frameOffset = 0;
    UInt32 framesRead = workingFrames;
    // Loop through input file, write to output file.
    while (framesRead != 0) {
        // # of frames to read
        framesRead = workingFrames;
        // Read into buffer
        OSStatus result = 0;
        result = ExtAudioFileSeek(inputFile, frameOffset);
        result = ExtAudioFileRead(inputFile, &framesRead, workBuffer);
        // ******** Process Callback *************
        AURenderCallback process = self.audioCallback.inputProc;
        if (process) {
            AudioUnitRenderActionFlags ioActionFlags;
            AudioTimeStamp inTimeStamp = {.mSampleTime = framesRead};
            UInt32 inBusNumber = 0;
            UInt32 inNumberFrames = workingFrames;
            
            process(self.audioCallback.inputProcRefCon,
                    &ioActionFlags,
                    &inTimeStamp,
                    inBusNumber,
                    inNumberFrames,
                    workBuffer);
        }
        // ****************************************
        
        Float32 *work = workBuffer->mBuffers[0].mData;
        for (int i = 0; i < framesRead; i++) {
            //NSLog(@"Work: %f", work[i]);
            work[i] = work[i] * 0.1;
        }
        // Write buffer to file
        result = ExtAudioFileWrite(destinationFile, framesRead, workBuffer);
        if (result) NSLog(@"Couldn't Write Frames to File: %d", (int)result);
        frameOffset = frameOffset + framesRead;
        //NSLog(@"Frame: %i %lli", framesRead, frameOffset);
    }
    // Close Files
    [AUDFileUtil closeAudioFile:inputFile DidModify:NO];
    [AUDFileUtil closeAudioFile:destinationFile DidModify:YES];
    // Cleanup work buffer
    [AUDUtil destroyBufferList:workBuffer];
}

- (void)processFile
{
    
}

//+ (UInt32)framesInBuffer:(AudioBufferList*)buffer
//                WithDesc:(AudioStreamBasicDescription)asbd
//{
//    // Get # of frames to copy.
//    UInt32 bufferSize = buffer->mBuffers[0].mDataByteSize;
//    // TODO: Calc different for Stereo?
//    UInt32 frameSize = asbd.mBytesPerFrame;
//
//    UInt32 frameCount = bufferSize / frameSize;
//    return frameCount;
//}

//- (OSStatus)loadAudioFile:(ExtAudioFileRef)xafref
//                 WithDesc:(AudioStreamBasicDescription)fileASBD
//                   Frames:(SInt64)frames
//                 toBuffer:(AudioBufferList*)outBufferList
//                     Size:(SInt64*)outSize
//{
//	// set the client format to be what we want back
//    double rateRatio = 1.0;
//    rateRatio = 44100.0 / fileASBD.mSampleRate;
//	fileASBD.mSampleRate = 44100.0;
//
//	// setup the output file format
//    [AUDUtil streamFormatMonoInt:&fileASBD SampleRate:44100];
//    OSStatus result;
//	UInt32 propSize = sizeof(fileASBD);
//	result = ExtAudioFileSetProperty(xafref,
//                                     kExtAudioFileProperty_ClientDataFormat,
//                                     propSize,
//                                     &fileASBD);
//	if (result) { printf("Set Ext ClientDataFormat %d %08X %4.4s\n",
//                         (int)result, (int)result, (char*)&result); return result; }
//
//	// get the file's length in sample frames
//	SInt64 numFrames = 0;
//	propSize = sizeof(numFrames);
//	result = ExtAudioFileGetProperty(xafref,
//                                     kExtAudioFileProperty_FileLengthFrames,
//                                     &propSize,
//                                     &numFrames);
//	if (result) { printf("Get Ext FileLengthFrames result %d %08X %4.4s\n",
//                         (int)result, (int)result, (char*)&result); return result; }
//	numFrames = (UInt32)(numFrames * rateRatio); // sample rate conversion
//	NSLog(@"numframe: %lli %f", numFrames, rateRatio);
//
//	// read audio data out of the file into our data buffer
//	UInt32 numPackets = (UInt32)numFrames;
//
//    UInt32 bSize = outBufferList->mBuffers[0].mDataByteSize;
//    NSLog(@"NUMPACK: %i %i", numPackets, bSize);
//	result = ExtAudioFileRead(xafref, &numPackets, outBufferList);
//    bSize = outBufferList->mBuffers[0].mDataByteSize;
//    NSLog(@"buffBBBB: %i %i", numPackets, bSize);
//
//	*outSize = numFrames;
//	return result;
//}

//- (void)saveBufferList:(AudioBufferList*)bufferList
//            streamDesc:(AudioStreamBasicDescription)streamASBD
//                toFile:(NSString*)filePath
//              fileDesc:(AudioStreamBasicDescription)fileASBD
//                  Size:(UInt64)size
//{
//    
//    SInt32 frames = [AUDOfflineRender framesInBuffer:bufferList
//                                            WithDesc:streamASBD];
//    // Create File
//    destinationFile = [self createNewFile:filePath
//                                 FileASBD:streamASBD
//                           WithStreamASBD:fileASBD];
//    // Write Data
//    [self writeBuffer:bufferList
//               Frames:frames
//               ToFile:destinationFile];
//    // Close File
//    [self closeAudioFile:destinationFile DidModify:YES];
//    // Clean up buffer
//    [AUDUtil destroyBufferList:bufferList];
//}

//// In loop, Use C function directly, avoid using obj-c in a callback
//- (void)writeBuffer:(AudioBufferList*)ioData
//             Frames:(UInt32)numFrames
//             ToFile:(ExtAudioFileRef)extDestFile
//{
//    OSStatus result = 0;
//    result = ExtAudioFileWrite(extDestFile, numFrames, ioData);
//    if (result) NSLog(@"Couldn't Write Frames to File: %d", (int)result);
//}


@end
