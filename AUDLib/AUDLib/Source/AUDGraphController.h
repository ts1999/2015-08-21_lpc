//
//  AUDGraphController.h
//  AUDLib
//
//  Created by Tim Bolstad on 12/5/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioSession.h>
#import "AUDUtil.h"

@interface AUDGraphController : NSObject
{
    // Audio Graph Members
    AUGraph   mGraph;
    AudioUnit mMixer;
    AudioUnit mInputOutputUnit;
    AUNode mMixerNode;    
    AUNode mInputOutputNode;
}

@property (nonatomic) Float64 sampleRate;
@property (nonatomic) Float64 bufferLengthSec;
@property (nonatomic, readonly) BOOL isRunning;
@property (nonatomic, readonly) AudioUnit ioUnit;
//// AVAudioSessionInterruptions
//@property (nonatomic) NSInteger interrupted;
//@property (nonatomic) NSInteger runningPriorToInterruption;

// Default Setup
- (void)basicSetup;
- (void)basicSetupWithPreferredSampleRate:(double)sampleRate;

// Audio Initialization
- (NSError*)setAudioSessionActive:(BOOL)active
                         Category:(NSString*)category
                  CategoryOptions:(AVAudioSessionCategoryOptions)categoryOptions
                    ActiveOptions:(AVAudioSessionSetActiveOptions)activeOptions;

- (NSError*)setAudioSessionActive:(BOOL)active
                         Category:(NSString*)category
                  CategoryOptions:(AVAudioSessionCategoryOptions)categoryOptions
                    ActiveOptions:(AVAudioSessionSetActiveOptions)activeOptions
              PreferredSampleRate:(double)sampleRate;

- (void)setAudioSessionInactive;
- (void)initializeAUGraphWithMixerBusCount:(UInt32)numBuses
                               EnableInput:(BOOL)enableInput;

// Audio On/Off
- (BOOL)toggleSound;
- (void)startGraph;
- (void)stopGraph;
- (void)updateGraph;

// Mixer
- (OSStatus) connectAudioCallback:(AURenderCallbackStruct)callbackStruct
                  toMixerInputBus:(UInt32)inputBus;
- (Float32)volumeAtIndex:(UInt32)index;
- (void)setVolume:(Float32)value atIndex:(UInt32)index;
- (Float32)panAtIndex:(UInt32)index;
- (void)setPan:(Float32)value atIndex:(UInt32)index;
- (Float32)getMixerOutputVolume;

@end
