//
//  AUDOfflineRender.h
//  AUDLib
//
//  Created by Tim Bolstad on 12/23/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface AUDOfflineRender : NSObject

@property (nonatomic) AURenderCallbackStruct audioCallback;

- (void)processInputFile:(NSString*)sourceURLString
              OutputFile:(NSString*)destinationURLString;

@end
