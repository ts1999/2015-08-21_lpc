//
//  AUDTestSine.h
//  AUDLib
//
//  Created by Tim Bolstad on 12/7/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@class AUDGraphController;

@interface AUDTestSine : NSObject
{
    Float32 sinPhase; // Oscillator phase
}

@property (nonatomic) Float64 sampleRate;
@property (nonatomic) Float32 frequency;

- (AURenderCallbackStruct)audioCallback;
- (void)addToGraph:(AUDGraphController*)graphController
      AtMixerIndex:(NSInteger)mixerIndex;

@end
