//
//  AUDTestInput.m
//  AUDLib
//
//  Created by Tim Bolstad on 12/7/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import "AUDInput.h"
#import "AUDGraphController.h"

@implementation AUDInput

- (id)init
{
    if (self = [super init]) {

    }
    return self;
}

- (void)addToGraph:(AUDGraphController*)graphController
      AtMixerIndex:(NSInteger)mixerIndex
{
    AURenderCallbackStruct callbackStruct = [self audioCallback];
    mInputOutputUnit = graphController.ioUnit;
    [graphController connectAudioCallback:callbackStruct
                          toMixerInputBus:0];
    [graphController updateGraph];
}

static OSStatus micInput(void *inRefCon,
                         AudioUnitRenderActionFlags *ioActionFlags,
                              const AudioTimeStamp *inTimeStamp,
                              UInt32 inBusNumber,
                              UInt32 inNumberFrames,
                              AudioBufferList *ioData)
{
	// Get a reference to the object that was passed with the callback
	AUDInput *THIS = (__bridge AUDInput*)inRefCon;
    AudioUnit remoteIO = THIS->mInputOutputUnit;
    
    OSStatus renderErr = noErr;
	UInt32 bus1 = 1; // Bus 1 on remoteIO == mic / audio hardware input
    renderErr = AudioUnitRender(remoteIO,
                                ioActionFlags,
                                inTimeStamp,
                                bus1,
                                inNumberFrames,
                                ioData);
    
	return renderErr;
}

// Bundle up the callback and the reference pointer
- (AURenderCallbackStruct)audioCallback
{
    // Callback Struct
	AURenderCallbackStruct renderCallbackStruct;
    // Function Pointer
	renderCallbackStruct.inputProc = &micInput;
	// Object Reference
    renderCallbackStruct.inputProcRefCon = (__bridge void*)self;
	return renderCallbackStruct;
}

@end
