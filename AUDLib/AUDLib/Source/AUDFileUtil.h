//
//  AUDFileUtil.h
//  AUDLib
//
//  Created by Tim Bolstad on 12/26/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AUDUtil.h"

@interface AUDFileUtil : NSObject


+ (AudioFileID)fileIDForExtAudioFile:(ExtAudioFileRef)fileRef;
+ (OSStatus)openAudioFile:(NSString*)filePath
               OutFileRef:(ExtAudioFileRef*)fileRef
                  OutDesc:(AudioStreamBasicDescription*)asbd;
+ (ExtAudioFileRef)createNewFile:(NSString*)filePath
                        FileASBD:(AudioStreamBasicDescription)fileASBD
                  WithStreamASBD:(AudioStreamBasicDescription)streamASBD;
+ (void)closeAudioFile:(ExtAudioFileRef)extAudioFile
             DidModify:(BOOL)didModify;
+ (SInt64)fileFrameLength:(ExtAudioFileRef)fileRef;

@end
