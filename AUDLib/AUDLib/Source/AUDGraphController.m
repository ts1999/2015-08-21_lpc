//
//  AUDGraphController.m
//  AUDLib
//
//  Created by Tim Bolstad on 12/5/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import "AUDGraphController.h"

#define PREF_BUFFER_DUR (512)

@implementation AUDGraphController

- (AudioUnit)ioUnit
{
    return mInputOutputUnit;
}

- (void)basicSetupWithPreferredSampleRate:(double)sampleRate
{
    NSString *sessionCategory = AVAudioSessionCategoryPlayAndRecord;
    AVAudioSessionCategoryOptions categoryOptions = AVAudioSessionCategoryOptionMixWithOthers;
    AVAudioSessionSetActiveOptions activeOptions = AVAudioSessionSetActiveOptionNotifyOthersOnDeactivation;
    
    [self setAudioSessionActive:TRUE
                       Category:sessionCategory
                CategoryOptions:categoryOptions
                  ActiveOptions:activeOptions
            PreferredSampleRate:sampleRate];
    
    [self initializeAUGraphWithMixerBusCount:1
                                 EnableInput:TRUE];
}

- (void)basicSetup
{
    [self basicSetupWithPreferredSampleRate:44100.0];
}

#pragma mark AVAudioSession
- (NSError*)setAudioSessionActive:(BOOL)active
                         Category:(NSString*)category
                  CategoryOptions:(AVAudioSessionCategoryOptions)categoryOptions
                    ActiveOptions:(AVAudioSessionSetActiveOptions)activeOptions
{
    return [self setAudioSessionActive:active
                              Category:category
                       CategoryOptions:categoryOptions
                         ActiveOptions:activeOptions
                   PreferredSampleRate:44100.0];
}

- (NSError*)setAudioSessionActive:(BOOL)active
                         Category:(NSString*)category
                  CategoryOptions:(AVAudioSessionCategoryOptions)categoryOptions
                    ActiveOptions:(AVAudioSessionSetActiveOptions)activeOptions
              PreferredSampleRate:(double)sampleRate
{
    AVAudioSession *session = [AVAudioSession sharedInstance];
    
    //[session setPreferredSampleRate: [[AVAudioSession sharedInstance] sampleRate]];
    // Set Audio category
    NSError *setCategoryError = nil;
    [session setCategory:category
                   error:&setCategoryError];
    if (setCategoryError) {NSLog(@"ERROR setting Audio Session");}

    
    // Activate Session
    NSError *activationError = nil;
    [session setActive:active
           withOptions:activeOptions
                 error:&activationError];
    if (activationError) {NSLog(@"ERROR activating AVAudioSession");}

    
    // set sample rate
    NSError *setPrefSampleRateError = nil;
    BOOL success = [session setPreferredSampleRate:sampleRate error:&setPrefSampleRateError];
    if (!success || setPrefSampleRateError) { NSLog(@"ERROR setting preferred sample rate: %@",setPrefSampleRateError); }
    
    self.sampleRate = session.sampleRate;
//    NSLog(@"\t\ttrying to set sample rate to %f",sampleRate);
//    NSLog(@"\t\tactual session sample rate is %f",self.sampleRate);

    NSError *setPrefBufferDurError = nil;
    NSTimeInterval bufferDur = PREF_BUFFER_DUR / self.sampleRate;
    success = [session setPreferredIOBufferDuration:bufferDur error:&setPrefBufferDurError];
    if (!success || setPrefBufferDurError) { NSLog(@"ERROR setting preferred buffer duration: %@",setPrefBufferDurError); }
    
    // buffer length (seconds)
    self.bufferLengthSec = (Float64)session.IOBufferDuration;

    return activationError;
}

-(void) setAudioSessionInactive {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    NSCheck([session setActive: NO error:  &err]);
}

#pragma mark AUGraph
- (void)initializeAUGraphWithMixerBusCount:(UInt32)numBuses
                               EnableInput:(BOOL)enableInput
{
	//************************************************************
	//*** Setup the AUGraph, add AUNodes, and make connections ***
	//************************************************************
    
	// create a new AUGraph
    CAError(NewAUGraph(&mGraph), "Failed to create new graph.");
    
    // Create AudioComponentDescriptions for the AUs we want in the graph
    // mixer component
	AudioComponentDescription mixer_desc;
	mixer_desc.componentType = kAudioUnitType_Mixer;
	mixer_desc.componentSubType = kAudioUnitSubType_MultiChannelMixer;
	mixer_desc.componentFlags = 0;
	mixer_desc.componentFlagsMask = 0;
	mixer_desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    
	//  output component
	AudioComponentDescription output_desc;
	output_desc.componentType = kAudioUnitType_Output;
	output_desc.componentSubType = kAudioUnitSubType_RemoteIO;
	output_desc.componentFlags = 0;
	output_desc.componentFlagsMask = 0;
	output_desc.componentManufacturer = kAudioUnitManufacturer_Apple;
    
    // Add nodes to the graph to hold our AudioUnits,
	CAError(AUGraphAddNode(mGraph, &output_desc, &mInputOutputNode),
            "Add new output failed.");
	CAError(AUGraphAddNode(mGraph, &mixer_desc, &mMixerNode ),
            "Add new mixer failed.");
    
    // Connect the mixer node's output to the output node's input
	CAError(AUGraphConnectNodeInput(mGraph, mMixerNode, 0, mInputOutputNode, 0),
            "Mixer -> Output connection failed.");
    
    // open the graph AudioUnits are open but not initialized
    // (no resource allocation occurs here)
	CAError(AUGraphOpen(mGraph), "Add new mixer failed");
    
	// Get a link to the AU's so we can talk to them later
	CAError(AUGraphNodeInfo(mGraph, mMixerNode, NULL, &mMixer),
            "Get mixer info failed.");
	CAError(AUGraphNodeInfo(mGraph, mInputOutputNode, NULL, &mInputOutputUnit),
            "Get output info failed");
    
    // Set Max frames in buffer
    UInt32 maxFrames = 4096;
    CAError(AudioUnitSetProperty(mInputOutputUnit,
                                 kAudioUnitProperty_MaximumFramesPerSlice,
                                 kAudioUnitScope_Global,
                                 0,
                                 &maxFrames,
                                 sizeof(maxFrames)),
            "Failed to set Max Frames Per Slice.");;
    
    if (enableInput) {
        // Enable microphone input for recording
        UInt32 enableFlag = 1;
        AudioUnitScope inputBus = 1;
        AudioUnitSetProperty(mInputOutputUnit,
                             kAudioOutputUnitProperty_EnableIO,
                             kAudioUnitScope_Input,
                             inputBus,
                             &enableFlag,
                             sizeof(enableFlag));
        // Enable metering
        UInt32 meterEnable = 1;
        AudioUnitSetProperty(mMixer,
                             kAudioUnitProperty_MeteringMode,
                             kAudioUnitScope_Output,
                             0,
                             &meterEnable,
                             sizeof(UInt32));
        
        #if TARGET_IPHONE_SIMULATOR
        // Configure input stream format
        AudioStreamBasicDescription inputASBD = {0};
        UInt32 inputASBDSize = sizeof(inputASBD);
        [AUDUtil streamFormatStereoFloat:&inputASBD
                                 SampleRate:self.sampleRate];
        
        CAError(AudioUnitSetProperty (mInputOutputUnit,
                                      kAudioUnitProperty_StreamFormat,
                                      kAudioUnitScope_Output,
                                      inputBus,
                                      &inputASBD,
                                      inputASBDSize),
                    "Failed to set input stream ");
        #endif
    }
    
    // Set bus count on mixer.
	UInt32 numBusSize = sizeof(numBuses);
    CAError(AudioUnitSetProperty(mMixer,
                                 kAudioUnitProperty_ElementCount,
                                 kAudioUnitScope_Input,
                                 0,
                                 &numBuses,
                                 numBusSize),
            "Failed to set mixer bus count.");
    
    CAError(AUGraphInitialize(mGraph), "Failed to Initialize AUGraph.");
    
    
    // Configure stream format
    AudioStreamBasicDescription mixerASBD = {0};
    UInt32 mixerASBDSize = sizeof(mixerASBD);
    [AUDUtil streamFormatStereoFloat:&mixerASBD
                             SampleRate:self.sampleRate];
    // Set the mixer input stream format.
    CAError(AudioUnitSetProperty(mMixer,
                                 kAudioUnitProperty_StreamFormat,
                                 kAudioUnitScope_Input,
                                 0,
                                 &mixerASBD,
                                 mixerASBDSize),
            "Failed to set mixer stream ");
}

- (void)updateGraph
{
    Boolean isUpdated = FALSE;
    OSStatus result = AUGraphUpdate(mGraph, &isUpdated);
	// Print the result
	if (result) { printf("AUGraphUpdate result %d %08X %4.4s\n", (int)result, (int)result, (char*)&result); return; }
}

- (BOOL)isRunning
{
    Boolean isRunningFlag = false;
    // Check to see if the graph is running.
    AUGraphIsRunning(mGraph, &isRunningFlag);
    
    return isRunningFlag;
}

- (BOOL)toggleSound
{
    BOOL isActive = self.isRunning;
    if (isActive) {
        [self stopGraph];
    } else {
        [self startGraph];
    }
    return self.isRunning;
}

// starts render
- (void)startGraph
{
	// Start the AUGraph
	OSStatus result = AUGraphStart(mGraph);
	// Print the result
	if (result) { printf("AUGraphStart result %d %08X %4.4s\n", (int)result, (int)result, (char*)&result); return; }
}

// stops render
- (void)stopGraph
{
    Boolean isRunning = false;
    // Check to see if the graph is running.
    OSStatus result = AUGraphIsRunning(mGraph, &isRunning);
    // If the graph is running, stop it.
    if (isRunning) {
        result = AUGraphStop(mGraph);
    }
	if (result) { printf("AUGraphEnd result %d %08X %4.4s\n", (int)result, (int)result, (char*)&result); return; }
}

#pragma mark Mixer Connections
- (OSStatus) connectAudioCallback:(AURenderCallbackStruct)callbackStruct
                  toMixerInputBus:(UInt32)inputBus
{
	OSStatus result;
	
	// Set a callback for the specified node's specified input
	result = AUGraphSetNodeInputCallback(mGraph, mMixerNode, inputBus, &callbackStruct);
    if (result) { printf("Failed to set mixer callback. %d %08X %4.4s\n", (int)result, (int)result, (char*)&result); return result; }
	
	Boolean isUpdated = FALSE;
	result = AUGraphUpdate(mGraph, &isUpdated);
    
    if (result) { printf("Failed to update AUGraph. %d %08X %4.4s\n", (int)result, (int)result, (char*)&result); return result; }
    //	if (isUpdated == false) { // TODO: better checks
    //		NSLog(@"Could Not Connect Node %li", result);
    //	}
	return result;
}

#pragma mark - Mixer Controls
- (Float32) volumeAtIndex:(UInt32)index
{
    Float32 value;
    AudioUnitGetParameter(mMixer,
                          kMultiChannelMixerParam_Volume,
                          kAudioUnitScope_Input,
                          index,
                          &value);
	return value;
}

- (void) setVolume:(Float32)value atIndex:(UInt32)index
{
    AudioUnitSetParameter(mMixer,
                          kMultiChannelMixerParam_Volume,
                          kAudioUnitScope_Input,
                          index, value, 0);
}

- (Float32) panAtIndex:(UInt32)index
{
	Float32 pan;
    AudioUnitGetParameter(mMixer,
                          kMultiChannelMixerParam_Pan,
                          kAudioUnitScope_Input,
                          index,
                          &pan);
	return pan;
}

- (void) setPan:(Float32)value atIndex:(UInt32)index
{
    AudioUnitSetParameter(mMixer,
                          kMultiChannelMixerParam_Pan,
                          kAudioUnitScope_Input,
                          index, value, 0);
}

- (float)getMixerOutputVolume
{
	Float32 value;
    AudioUnitGetParameter(mMixer,
                          kMultiChannelMixerParam_PostAveragePower,
                          kAudioUnitScope_Output,
                          0,
                          &value);
	return value;
}

#pragma mark - dealloc
// Clean up memory
- (void)dealloc
{
    [self stopGraph];
    DisposeAUGraph(mGraph);
}

@end
