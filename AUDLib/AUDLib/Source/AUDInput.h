//
//  AUDTestInput.h
//  AUDLib
//
//  Created by Tim Bolstad on 12/7/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
@class AUDGraphController;

@interface AUDInput : NSObject
{
    AudioUnit mInputOutputUnit;
}

- (void)addToGraph:(AUDGraphController*)graphController
      AtMixerIndex:(NSInteger)mixerIndex;

@end
