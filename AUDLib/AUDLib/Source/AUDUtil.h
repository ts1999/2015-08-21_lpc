//
//  AUDUtility.h
//  AUDLib
//
//  Created by Tim Bolstad on 12/8/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

void CAError(OSStatus error, const char *operation);
#define NSCheck(expr) do { NSError *err = nil; if (!(expr)) { NSLog(@"error from %s: %@", #expr, err);  exit(1); } } while (0)

@interface AUDUtil : NSObject

// AudioStreamBasicDescription Methods
+ (AudioBufferList*) bufferListWithChannels:(UInt32)numChannels
                                         andSize:(UInt32)size
                                 allocateBuffers:(BOOL)allocate;
+ (void) destroyBufferList:(AudioBufferList*)list;
+ (void) streamFormatStereoFloat:(AudioStreamBasicDescription*)asbd
                      SampleRate:(Float64)sampleRate;
+ (void) streamFormatMonoFloat:(AudioStreamBasicDescription*)asbd
                    SampleRate:(Float64)sampleRate;
+ (void) streamFormatMonoInt:(AudioStreamBasicDescription*)asbd
                  SampleRate:(Float64)sampleRate;
+ (void)printASBD:(AudioStreamBasicDescription)asbd;

+ (void)printTestBuffer:(AudioBufferList*)ioData;

+ (void)convertIntBuffer:(SInt16**)inBuffer
           ToFloatBuffer:(Float32**)outBuffer
                    Size:(UInt64*)outSize;

@end
