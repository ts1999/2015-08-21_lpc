//
//  AUDAppDelegate.m
//  AUDLib
//
//  Created by Tim Bolstad on 12/5/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import "AUDAppDelegate.h"
#import "AUDViewController.h"
#import "AUDGraphController.h"
#import "AUDTestSine.h"
#import "AUDInput.h"
#import "AUDOfflineRender.h"

@interface AUDAppDelegate ()
{
    CFURLRef sourceURL;
    CFURLRef destinationURL;
}

@end

@implementation AUDAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

#define OFFLINE_TEST    
//#define SINE_TEST
//#define INPUT_TEST
    
#ifdef OFFLINE_TEST
    
    [self testOfflineRenderExample];
    
#else //
    
    // Create audio controller
    self.audioController = [[AUDGraphController alloc] init];
    [self.audioController basicSetup];
    
    // Add custom audio processing
    #ifdef MIC_TEST
    self.testInput = [[AUDTestInput alloc] init];
    [self.testInput addToGraph:self.audioController
                  AtMixerIndex:0];
    #endif
    
    #ifdef SINE_TEST
    self.testSine = [[AUDTestSine alloc] init];
    [self.testSine addToGraph:self.audioController
                 AtMixerIndex:0];
    #endif

    // Start Audio
    [self.audioController startGraph];
    // Pass references to view controller.
    AUDViewController *rootVC = (AUDViewController*)self.window.rootViewController;
    rootVC.audioController = self.audioController;
#endif
    


    return YES;
}

- (void)testOfflineRenderExample
{
    //self.testSine = [[AUDTestSine alloc] init];
    
    self.offlineRender = [[AUDOfflineRender alloc] init];
    self.offlineRender.audioCallback = self.testSine.audioCallback;
    
    // create the URLs we'll use for source and destination
    NSString *sourceString = [[NSBundle mainBundle] pathForResource:@"break" ofType:@"wav"];
    
    NSArray  *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *destinationFilePath = [[NSString alloc] initWithFormat: @"%@/output.wav", documentsDirectory];
    
    [self.offlineRender processInputFile:sourceString
                              OutputFile:destinationFilePath];
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
