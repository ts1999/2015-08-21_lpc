//
//  AUDAppDelegate.h
//  AUDLib
//
//  Created by Tim Bolstad on 12/5/13.
//  Copyright (c) 2013 Tim Bolstad. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AUDTestSine;
@class AUDInput;
@class AUDGraphController;
@class AUDOfflineRender;

@interface AUDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) AUDTestSine *testSine;
@property (strong, nonatomic) AUDInput *testInput;
@property (strong, nonatomic) AUDGraphController *audioController;
@property (strong, nonatomic) AUDOfflineRender *offlineRender;

@end
