/**
 * @file Mutex.h
 * @Author Jon Forsyth
 * @date 2/20/14
 * @brief Thread lock. Copied from MoMutex.h in MoMu library.
 */

#ifndef __lpc_app__Mutex__
#define __lpc_app__Mutex__

#include <iostream>
#include <pthread.h>


typedef pthread_mutex_t MUTEX;

struct MoMutex
{
public:
    MoMutex();
    ~MoMutex();
    
public:
    void acquire( );
    void release(void);
    
protected:
    MUTEX mutex;
};




#endif /* defined(__lpc_app__Mutex__) */
