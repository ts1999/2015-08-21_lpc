//
//  LPCWordPracticeReader.m
//  lpc_app
//
//  Created by Sam Tarakajian on 2/2/16.
//
//

#import "LPCWordPracticeReader.h"
#import "CHCSVParser.h"

@interface LPCWordPracticeReader () <CHCSVParserDelegate>
@property (nonatomic, assign) NSInteger currentWordIndex;
@property (nonatomic, strong) NSMutableArray *wordEntries;
+ (NSURL *) defaultCSVFileURL;
@end

@implementation LPCWordPracticeReader

+ (NSURL *) defaultCSVFileURL
{
    return [[NSBundle mainBundle] URLForResource:@"staRt_wordlist" withExtension:@"csv"];
}

+ (instancetype) defaultWordPracticeReader
{
    NSArray *unscrambledWords = [NSArray arrayWithContentsOfCSVURL:[LPCWordPracticeReader defaultCSVFileURL]
                                                           options:CHCSVParserOptionsUsesFirstLineAsKeys];
    
    LPCWordPracticeReader *reader = [[LPCWordPracticeReader alloc] init];
    reader.wordEntries = [NSMutableArray arrayWithArray:unscrambledWords];
    [reader scrambleWords];
    reader.currentWordIndex = 0;
    return reader;
}

- (void) scrambleWords
{
    for (int i=0; i<self.wordEntries.count; ++i) {
        int randJ = arc4random_uniform(self.wordEntries.count);
        [self.wordEntries exchangeObjectAtIndex:i withObjectAtIndex:randJ];
    }
}

- (NSString *) currentWord
{
    NSArray *wordEntry = [self.wordEntries objectAtIndex:self.currentWordIndex];
    return [wordEntry objectAtIndex:0];
}

- (void) advance
{
    self.currentWordIndex = (self.currentWordIndex + 1) % self.wordEntries.count;
    if (self.currentWordIndex == 0)
        [self scrambleWords];
}

@end
