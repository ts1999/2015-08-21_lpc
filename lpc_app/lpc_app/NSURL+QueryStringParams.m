//
//  NSURL+QueryStringParams.m
//  lpc_app
//
//  Created by Sam Tarakajian on 2/25/16.
//
//

#import "NSURL+QueryStringParams.h"

@implementation NSURL (QueryStringParams)

- (NSString *)valueForKey:(NSString *)key
           fromQueryItems:(NSArray *)queryItems
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name=%@", key];
    NSURLQueryItem *queryItem = [[queryItems
                                  filteredArrayUsingPredicate:predicate]
                                 firstObject];
    return queryItem.value;
}

- (NSString *) firstParamValueForKey:(NSString *)key
{
    NSURLComponents *urlComponents = [NSURLComponents componentsWithURL:self
                                                resolvingAgainstBaseURL:NO];
    NSArray *queryItems = urlComponents.queryItems;
    return [self valueForKey:key fromQueryItems:queryItems];
}

@end
