//
//  LPCWordPracticeSession.h
//  lpc_app
//
//  Created by Sam Tarakajian on 2/2/16.
//
//

#import <Foundation/Foundation.h>
#import "LPCUploadable.h"

@class LPCAccount;
@class LPCRecordingSession;

typedef NS_ENUM(NSInteger, LPCWordPracticeRating) {
    LPCWordPracticeRatingNone,
    LPCWordPracticeRatingLow,
    LPCWordPracticeRatingMedium,
    LPCWordPracticeRatingHigh
};

@interface LPCWordPracticeSession : NSObject <NSCoding, LPCUploadable>
@property (nonatomic, readonly) NSString *ratingsFilename;
@property (nonatomic, assign) LPCWordPracticeRating ratingForCurrentWord;
@property (nonatomic, strong) LPCRecordingSession *recordingSession;
@property (nonatomic, readonly) NSUInteger currentWordIndex;

+ (instancetype) sessionWithAccount:(LPCAccount *)account;

- (void) beginSession;
- (void) endSession;

- (NSString *) currentWord;
- (void) advance;

@end
