//
//  LPCAccountManager.m
//  lpc_app
//
//  Created by Sam Tarakajian on 12/2/15.
//
//

#import "LPCAccountManager.h"
#import "LPCRecordingSession.h"
#import "LPCWordPracticeSession.h"
#import "CHCSVParser.h"

NSString *const LPCAccountKeyName = @"name";
NSString *const LPCAccountKeyAge = @"age";
NSString *const LPCAccountKeyGender = @"gender";
NSString *const LPCAccountKeyHeightFeet = @"heightFeet";
NSString *const LPCAccountKeyHeightInches = @"heightInches";

#define kUUIDKey                @"UUID"
#define kMetadataKey            @"Metadata"
#define kRecordingsKey          @"Recordings"
#define kWordPracticesKey       @"WordPractices"
#define kAccountsKey            @"Accounts"
#define kCurrentAccountUUIDKey  @"CurrentAccountUUID"
#define kMaxAgeKey              @"MaxAge"
#define kMinAgeKey              @"MinAge"

static NSDictionary *s_f3TargetDictMale = nil;
static NSDictionary *s_f3TargetDictFemale = nil;

@interface LPCAccount ()
@property (nonatomic, strong) NSString *uuid;
@property (nonatomic, strong) NSMutableDictionary *mutableMetadata;
@property (nonatomic, strong) NSMutableArray<LPCRecordingSession *> *mutableRecordings;
@property (nonatomic, strong) NSMutableArray<LPCWordPracticeSession *> *mutableWordPractices;
+ (instancetype) newUniqueAccount;
@end

@implementation LPCAccount

+ (instancetype) newUniqueAccount
{
    LPCAccount *account = [[LPCAccount alloc] init];
    account.uuid = [[NSUUID UUID] UUIDString];
    account.mutableMetadata = [[NSMutableDictionary alloc] init];
    account.mutableRecordings = [NSMutableArray array];
    account.mutableWordPractices = [NSMutableArray array];
    return account;
}

+ (void) initialize
{
    NSString *f3CSVPath = [[NSBundle mainBundle] pathForResource:@"F3r_norms_Lee_et_al_1999" ofType:@"csv"];
    NSURL *f3CSVURL = [NSURL fileURLWithPath:f3CSVPath];
    
    NSError *error = nil;
    NSArray<CHCSVOrderedDictionary *> *rows = [NSArray arrayWithContentsOfCSVURL:f3CSVURL options:CHCSVParserOptionsUsesFirstLineAsKeys];
    if (rows == nil) {
        //something went wrong; log the error and exit
        NSLog(@"error parsing file: %@", error);
        return;
    } else {
        NSMutableDictionary *male = [NSMutableDictionary dictionary];
        NSMutableDictionary *female = [NSMutableDictionary dictionary];
        NSInteger minAgeMale, minAgeFemale = NSIntegerMax;
        for (CHCSVOrderedDictionary *row in rows) {
            BOOL isMale = [[row objectForKey:@"Sex"] isEqualToString:@"M"];
            NSMutableDictionary *thisDict = (isMale ? male : female);
            NSInteger *minAgePtr = (isMale ? &minAgeMale : &minAgeFemale);
            NSString *ageString = [row objectForKey:@"Age"];
            if ([ageString containsString:@"+"]) {
                ageString = [ageString stringByReplacingOccurrencesOfString:@"+" withString:@""];
                [thisDict setObject:@([ageString integerValue]) forKey:kMaxAgeKey];
            }
            [thisDict setObject:row forKey:@([ageString integerValue])];
            if ([ageString integerValue] < *minAgePtr)
                *minAgePtr = [ageString integerValue];
        }
        
        [male setObject:@(minAgeMale) forKey:kMinAgeKey];
        [female setObject:@(minAgeFemale) forKey:kMinAgeKey];
        
        s_f3TargetDictMale = [NSDictionary dictionaryWithDictionary:male];
        s_f3TargetDictFemale = [NSDictionary dictionaryWithDictionary:female];
    }
}

+ (CHCSVOrderedDictionary *) rowForAge:(NSInteger)age gender:(NSString *)gender
{
    BOOL isMale = !([gender containsString:@"f"] || [gender containsString:@"F"]);
    NSDictionary *theDict = (isMale ? s_f3TargetDictMale : s_f3TargetDictFemale);
    NSInteger maxAge = [[theDict objectForKey:kMaxAgeKey] integerValue];
    NSInteger minAge = [[theDict objectForKey:kMinAgeKey] integerValue];
    if (age > [[theDict objectForKey:kMaxAgeKey] integerValue]) {
        return [theDict objectForKey:@(maxAge)];
    } else if (age < [[theDict objectForKey:kMinAgeKey] integerValue]) {
        return [theDict objectForKey:@(minAge)];
    } else {
        return [theDict objectForKey:@(age)];
    }
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        NSString *uuid = [aDecoder decodeObjectForKey:kUUIDKey];
        NSMutableDictionary *metadata = [aDecoder decodeObjectForKey:kMetadataKey];
        NSArray *recordings = [aDecoder decodeObjectForKey:kRecordingsKey];
        NSArray *wordPractices = [aDecoder decodeObjectForKey:kWordPracticesKey];
        self.uuid = uuid;
        self.mutableMetadata = [NSMutableDictionary dictionaryWithDictionary:metadata];
        self.mutableRecordings = [NSMutableArray arrayWithArray:( recordings ? recordings : @[] )];
        self.mutableWordPractices = [NSMutableArray arrayWithArray:( wordPractices ? wordPractices : @[] )];
    } return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.uuid forKey:kUUIDKey];
    [aCoder encodeObject:self.metadata forKey:kMetadataKey];
    [aCoder encodeObject:self.recordings forKey:kRecordingsKey];
    [aCoder encodeObject:self.wordPractices forKey:kWordPracticesKey];
}

- (NSDictionary *) metadata
{
    return [NSDictionary dictionaryWithDictionary:self.mutableMetadata];
}

- (NSString *) name
{
    if ([self.metadata objectForKey:LPCAccountKeyName])
        return [self.metadata objectForKey:LPCAccountKeyName];
    return @"Anonymous";
}

- (NSInteger) heightExpressedInInches
{
    BOOL hasFeet = [self.mutableMetadata.allKeys containsObject:LPCAccountKeyHeightFeet];
    BOOL hasInches = [self.mutableMetadata.allKeys containsObject:LPCAccountKeyHeightInches];
    if (hasFeet && hasInches) {
        NSInteger feet = [[self.mutableMetadata objectForKey:LPCAccountKeyHeightFeet] integerValue];
        NSInteger inches = [[self.mutableMetadata objectForKey:LPCAccountKeyHeightInches] integerValue];
        return 12*feet + inches;
    } else {
        return NSNotFound;
    }
}

- (LPCRecordingSession *)beginRecordingSession
{
    return [LPCRecordingSession sessionWithAccount:self];
}

- (NSArray<LPCRecordingSession *> *)recordings
{
    return [NSArray arrayWithArray:self.mutableRecordings];
}

- (NSArray<LPCWordPracticeSession *> *)wordPractices
{
    return [NSArray arrayWithArray:self.mutableWordPractices];
}

- (NSInteger) targetF3
{
    BOOL hasAge = [self.mutableMetadata.allKeys containsObject:LPCAccountKeyAge];
    BOOL hasGender = [self.mutableMetadata.allKeys containsObject:LPCAccountKeyGender];
    if (hasAge && hasGender) {
        CHCSVOrderedDictionary *row = [LPCAccount rowForAge:[[self.mutableMetadata objectForKey:LPCAccountKeyAge] integerValue]
                                                     gender:[self.mutableMetadata objectForKey:LPCAccountKeyGender]];
        return [[row objectForKey:@"F3_(Hz)"] integerValue];
    } else {
        return NSNotFound;
    }
}

- (NSInteger) stdevF3
{
    BOOL hasAge = [self.mutableMetadata.allKeys containsObject:LPCAccountKeyAge];
    BOOL hasGender = [self.mutableMetadata.allKeys containsObject:LPCAccountKeyGender];
    if (hasAge && hasGender) {
        CHCSVOrderedDictionary *row = [LPCAccount rowForAge:[[self.mutableMetadata objectForKey:LPCAccountKeyAge] integerValue]
                                                     gender:[self.mutableMetadata objectForKey:LPCAccountKeyGender]];
        return [[row objectForKey:@"F3_(stdev)_(Hz)"] integerValue];
    } else {
        return NSNotFound;
    }
}

- (NSInteger) targetLPCOrder
{
    BOOL hasAge = [self.mutableMetadata.allKeys containsObject:LPCAccountKeyAge];
    BOOL hasGender = [self.mutableMetadata.allKeys containsObject:LPCAccountKeyGender];
    NSInteger heightInInches = [self heightExpressedInInches];
    if (hasAge && hasGender && !(heightInInches == NSNotFound)) {
        NSString *gender = [self.mutableMetadata objectForKey:LPCAccountKeyGender];
        BOOL isMale = !([gender containsString:@"f"] || [gender containsString:@"F"]);
        BOOL is15OrOlder = [[self.mutableMetadata objectForKey:LPCAccountKeyAge] integerValue] >= 15;
        BOOL is64InchesOrTaller = heightInInches >= 64;
        
        // Man decision trees look like trash as nested if's
        if (isMale) {
            if (is15OrOlder) {
                if (is64InchesOrTaller) {
                    return 45;
                } else {
                    return 40;
                }
            } else {
                if (is64InchesOrTaller) {
                    return 40;
                } else {
                    return 35;
                }
            }
        } else {
            if (is15OrOlder) {
                if (is64InchesOrTaller) {
                    return 40;
                } else {
                    return 35;
                }
            } else {
                if (is64InchesOrTaller) {
                    return 35;
                } else {
                    return 30;
                }
            }
        }
    } else {
        return NSNotFound;
    }
}

@end

@interface LPCAccountManager ()
@property (nonatomic, strong) NSArray<LPCAccount*> *userAccounts;
@property (nonatomic, strong) LPCRecordingSession *currentRecordingSession;
@property (nonatomic, strong) LPCWordPracticeSession *currentWordPracticeSession;
@end

@implementation LPCAccountManager

+ (LPCAccountManager *) sharedInstance
{
    static LPCAccountManager *s_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_sharedInstance = [[LPCAccountManager alloc] init];
    });
    return s_sharedInstance;
}

- (id) init
{
    self = [super init];
    if (self) {
        NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
        NSData *encodedObject = [def objectForKey:kAccountsKey];
        if (!encodedObject) {
            self.userAccounts = [NSArray array];
        } else {
            self.userAccounts = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
        }
        NSString *currentAccountUUID = [def stringForKey:kCurrentAccountUUIDKey];
        if (currentAccountUUID)
            self.currentUserAccount = [self userAccountWithUUID:currentAccountUUID];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncUserAccounts)
                                                     name:UIApplicationWillResignActiveNotification
                                                   object:nil];
    } return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) syncUserAccounts
{
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:self.userAccounts];
    [def setObject:encodedObject forKey:kAccountsKey];
    [def synchronize];
}

- (LPCAccount *) createUserAccount
{
    LPCAccount *account = [LPCAccount newUniqueAccount];
    self.userAccounts = [self.userAccounts arrayByAddingObject:account];
    [self syncUserAccounts];
    return account;
}

- (LPCAccount *) userAccountWithUUID:(NSString *)uuid
{
    for (LPCAccount *account in self.userAccounts)
        if ([account.uuid isEqualToString:uuid])
            return account;
    return nil;
}

- (void) deleteUserAccount:(LPCAccount *)account
{
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.userAccounts];
    [array removeObject:account];
    self.userAccounts = [NSArray arrayWithArray:array];
    [self syncUserAccounts];
}

- (void) deleteAllUserAccounts
{
    self.userAccounts = [NSArray array];
    [self syncUserAccounts];
}

- (void) account:(LPCAccount *)account setMetadataValue:(nullable id)value forKey:(nonnull NSString *)key
{
    [account.mutableMetadata setObject:value forKey:key];
    [self syncUserAccounts];
}

- (void)setCurrentUserAccount:(LPCAccount *)currentUserAccount
{
    _currentUserAccount = currentUserAccount;
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:self.currentUserAccount.uuid forKey:kCurrentAccountUUIDKey];
    [def synchronize];
}

#pragma mark - Recording Session

- (LPCRecordingSession *) beginRecordingSession
{
    if (self.currentUserAccount) {
        self.currentRecordingSession = [LPCRecordingSession sessionWithAccount:self.currentUserAccount];
        [self.currentUserAccount.mutableRecordings addObject:self.currentRecordingSession];
        return self.currentRecordingSession;
    }
    return nil;
}

- (LPCRecordingSession *) endRecordingSession
{
    LPCRecordingSession *sesh = self.currentRecordingSession;
    self.currentRecordingSession = nil;
    [self syncUserAccounts];
    return sesh;
}

#pragma mark - Word Practice Session

- (LPCWordPracticeSession *) beginWordPracticeSession
{
    if (self.currentUserAccount) {
        self.currentWordPracticeSession = [LPCWordPracticeSession sessionWithAccount:self.currentUserAccount];
        [self.currentWordPracticeSession beginSession];
        [self.currentUserAccount.mutableWordPractices addObject:self.currentWordPracticeSession];
        return self.currentWordPracticeSession;
    }
    return nil;
}

- (LPCWordPracticeSession *) endWordPracticeSession
{
    LPCWordPracticeSession *sesh = self.currentWordPracticeSession;
    [sesh endSession];
    self.currentWordPracticeSession = nil;
    [self syncUserAccounts];
    return sesh;
}

@end
