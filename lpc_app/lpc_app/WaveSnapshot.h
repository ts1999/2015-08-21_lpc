//
//  WaveSnapshot.h
//  lpc_app
//
//  Created by moonbot on 9/11/14.
//
//

#import <Foundation/Foundation.h>

#import "AUDCustomSegue.h"
#import "lpc_appViewController.h"
#import "lpc_appAppDelegate.h"
 
@interface WaveSnapshot : NSObject

@property SceneVertex waveSnapshot;

-(id)init;

@end
