//
//  WaveSnapshot.m
//  lpc_app
//
//  Created by moonbot on 9/11/14.
//
//

#import "WaveSnapshot.h"

#import "lpc_appAppDelegate.h"
#import "lpc_appViewController.h"
#import "AUDInput.h"
#import "AUDGraphController.h"

@implementation WaveSnapshot

- (id)init
{
    self = [super init];
    if (self)
    {
        // superclass successfully initialized, further
        // initialization happens here ...
    }
    return self;
}

@end
