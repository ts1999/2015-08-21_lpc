//
//  LPCWordPracticeSession.m
//  lpc_app
//
//  Created by Sam Tarakajian on 2/2/16.
//
//

#import "LPCWordPracticeSession.h"
#import "LPCWordPracticeReader.h"
#import "LPCRecordingSession.h"
#import "LPCAccountManager.h"
#import "NSURL+DocumentsDirectory.h"
#import "CHCSVParser.h"

#define kRecordingSessionKey    @"RecordingSession"
#define kRatingsKey             @"Ratings"
#define kAccountUUIDKey         @"AccountUUID"
#define kAccountMetadataKey     @"AccountMetadata"
#define kDateKey                @"Date"

@interface LPCWordPracticeSession ()
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *dateString;
@property (nonatomic, strong) NSString *accountUUID, *ratingsFilename;
@property (nonatomic, strong) NSDictionary *accountMetadata;
@property (nonatomic, strong) LPCWordPracticeReader *wordPracticeReader;
@property (nonatomic, strong) CHCSVWriter *csvWriter;
@property (nonatomic, strong) NSNumber *currentTime;
@property (nonatomic, assign) NSUInteger currentWordIndex;
@end

@implementation LPCWordPracticeSession

+ (instancetype) sessionWithAccount:(LPCAccount *)account
{
    LPCWordPracticeSession *session = [[LPCWordPracticeSession alloc] init];
    session.accountUUID = account.uuid;
    session.accountMetadata = account.metadata;
    session.date = [NSDate date];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH-mm-ss"];
    
    session.dateString = [dateFormatter stringFromDate:session.date];
    session.ratingsFilename = [NSString stringWithFormat:@"%@-%@-ratings.csv", session.accountUUID, session.dateString];
    
    return session;
}

#pragma mark -

// Open up the file for writing and create the word reader for reading
- (void) beginSession
{
    NSURL *documentsDirectoryURL = [NSURL documentsDirectoryURL];
    NSString *ratingsPathString = [[documentsDirectoryURL URLByAppendingPathComponent:self.ratingsFilename] absoluteString];
    
    self.ratingForCurrentWord = LPCWordPracticeRatingNone;
    self.csvWriter = [[CHCSVWriter alloc] initForWritingToCSVFile:ratingsPathString];
    self.wordPracticeReader = [LPCWordPracticeReader defaultWordPracticeReader];
    [self.csvWriter writeLineOfFields:@[@"word", @"rating", @"timestamp"]];
    self.currentTime = [NSNumber numberWithDouble:CACurrentMediaTime()];
}

// Save the output file
- (void) endSession
{
    self.csvWriter = nil;
    self.wordPracticeReader = nil;
}

- (NSString *) currentWord
{
    return self.wordPracticeReader.currentWord;
}

// Write the current line, then advance
- (void) advance
{
    [self.csvWriter writeField:self.currentWord];
    [self.csvWriter writeField:[NSNumber numberWithInteger:self.ratingForCurrentWord]];
    [self.csvWriter writeField:self.currentTime];
    [self.csvWriter finishLine];
    [self.wordPracticeReader advance];
    self.ratingForCurrentWord = LPCWordPracticeRatingNone;
    self.currentTime = [NSNumber numberWithDouble:CACurrentMediaTime()];
    self.currentWordIndex++;
}

#pragma mark - NSCoding

- (id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self) {
        self.date = [[NSDate alloc] initWithCoder:aDecoder];
        self.ratingsFilename = [aDecoder decodeObjectForKey:kRatingsKey];
        if ([aDecoder containsValueForKey:kRecordingSessionKey])
            self.recordingSession = [aDecoder decodeObjectForKey:kRecordingSessionKey];
        self.accountUUID = [aDecoder decodeObjectForKey:kAccountUUIDKey];
        self.accountMetadata = [aDecoder decodeObjectForKey:kAccountMetadataKey];
        self.dateString = [aDecoder decodeObjectForKey:kDateKey];
    } return self;
}

- (void) encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.ratingsFilename forKey:kRatingsKey];
    [aCoder encodeObject:self.accountUUID forKey:kAccountUUIDKey];
    [aCoder encodeObject:self.accountMetadata forKey:kAccountMetadataKey];
    [aCoder encodeObject:self.dateString forKey:kDateKey];
    if (self.recordingSession)
        [aCoder encodeObject:self.recordingSession forKey:kRecordingSessionKey];
    [self.date encodeWithCoder:aCoder];
}

#pragma mark - LPCUploadable

- (NSString *) title
{
    return [NSString stringWithFormat:@"Word Practice Session:%@", self.ratingsFilename];
}

- (NSUInteger) numberOfAttachments
{
    return (self.recordingSession == nil) ? 1 : 4;
}

- (NSString *) nameForAttachmentAtIndex:(NSUInteger)index
{
    if (index == (self.numberOfAttachments-1)) {
        return self.ratingsFilename;
    } else {
        return [self.recordingSession nameForAttachmentAtIndex:index];
    }
}

- (NSString *) mimeTypeForAttachmentAtIndex:(NSUInteger)index
{
    if (index == (self.numberOfAttachments-1)) {
        return @"text/csv";
    } else {
        return [self.recordingSession mimeTypeForAttachmentAtIndex:index];
    }
}

- (NSData *) dataForAttachmentAtIndex:(NSUInteger)index
{
    if (index == (self.numberOfAttachments-1)) {
        NSData *data = nil;
        NSURL *dd = [NSURL documentsDirectoryURL];
        NSString *filename = [self nameForAttachmentAtIndex:index];
        if (filename) {
            NSURL *fileURL = [NSURL fileURLWithPath:[dd.absoluteString stringByAppendingPathComponent:filename]];
            data = [NSData dataWithContentsOfURL:fileURL];
        }
        return data;
    } else {
        return [self.recordingSession dataForAttachmentAtIndex:index];
    }
}

@end
