//
//  ViewController.h
//  lpc_app
//
//  Created by moonbot on 1/29/15.
//
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface ViewController : UIViewController

- (IBAction)didPressLPCToggle:(id)sender;
- (IBAction)didPressLPCRMoveRight:(id)sender;
- (void) resizeLPC: (int)x y:(int)y width:(int)width height:(int)height;
//- (int)addX:(int)x toY:(int)y

- (void) loadWebView: (id)sender;

@property (copy, nonatomic) NSURL* url;
//@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end
