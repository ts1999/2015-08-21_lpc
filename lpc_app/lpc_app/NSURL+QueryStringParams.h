//
//  NSURL+QueryStringParams.h
//  lpc_app
//
//  Created by Sam Tarakajian on 2/25/16.
//
//

#import <Foundation/Foundation.h>

@interface NSURL (QueryStringParams)

- (NSString *) firstParamValueForKey:(NSString *)key;

@end
