//
//  main.m
//  lpc_app
//

#import <UIKit/UIKit.h>

#import "lpc_appAppDelegate.h"

int main(int argc, char *argv[])
{
   @autoreleasepool 
   {
       return UIApplicationMain(argc, argv, nil, 
          NSStringFromClass([lpc_appAppDelegate class]));
   }
}
