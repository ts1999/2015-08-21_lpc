//
//  NSURL+DocumentsDirectory.h
//  lpc_app
//
//  Created by Sam Tarakajian on 2/3/16.
//
//

#import <Foundation/Foundation.h>

@interface NSURL (DocumentsDirectory)

+ (instancetype) documentsDirectoryURL;

@end
