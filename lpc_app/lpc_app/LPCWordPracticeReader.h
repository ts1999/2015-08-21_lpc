//
//  LPCWordPracticeReader.h
//  lpc_app
//
//  Created by Sam Tarakajian on 2/2/16.
//
//

#import <Foundation/Foundation.h>

@interface LPCWordPracticeReader : NSObject

+ (instancetype) defaultWordPracticeReader;

- (NSString *) currentWord;
- (void) advance;

@end
