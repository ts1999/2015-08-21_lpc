/**
 * @file lpc_appViewController.mm
 * @Author Jon Forsyth
 * @brief View controller
 */

#import "lpc_appViewController.h"
#import "lpc_appAppDelegate.h"
#import "LPCRecordingSession.h"

#include "LPCDisplayManager.h"
#include "AudioManager.h"
#include "Mutex.h"

#define PREF_SAMPLE_RATE (22050)  /**< preferred audio sample rate */

#define USE_LOCAL_FREQ_VERTS      /**< flag indicating that view controller should use OpenGL structures defined locally instead of those defined as member variables of LPCDisplayManager  */

@interface lpc_appViewController () {
    AudioManager *audioManager;             /**< instance of AudioManager object */
    LPCDisplayManager *displayManager;      /**< instance of LPCDisplayManager object */
    AUDInput *audInput;
}
@property (nonatomic, strong) LPCRecordingSession *currentRecordingSession;
@end

@implementation lpc_appViewController

@synthesize baseEffect;

#define MAX_NUM_TARG_FORMANTS (5)   /**< maximum number of LPC target formant frequencies */
#define NUM_LPC_DISPLAY_BINS (256)  /**< number of points in OpenGL structure used to draw LPC magnitude spectrum */

// maximum display frequency in Hz
#define MAX_DISPLAY_FREQ (4500)     /**< upper limit of LPC magnitude spectrum display (Hz) */

#ifdef USE_LOCAL_FREQ_VERTS
SceneVertex _freqVertices[NUM_LPC_DISPLAY_BINS];          /**< OpenGL vertices for drawing LPC magnitude spectrum */
SceneVertex _peakVertices[2*NUM_LPC_DISPLAY_BINS];        /**< OpenGL vertices for drawing peaks in LPC magnitude spectrum */
SceneVertex _targetFreqVertices[2*MAX_NUM_TARG_FORMANTS]; /**< OpenGL vertices containing points defining lines indicating target formant frequencies in LPC magnitude spectrum */
SceneVertex _underVerticies[2*256];                       /**< OpenGL vertices for drawing lines underneath LPC magnitude spectrum */
#endif

// May not need this after all...
SceneVertex _snapshotVerticies[256];                      /**< unused? */


MoMutex g_opengl_mutex;                                   /**< mutex lock for LPC computation */

double m_targetFormantFreqs[MAX_NUM_TARG_FORMANTS];       /**< array of target formant frequencies */
int m_targFormantIdx;                                     /**< index into m_targetFormantFreqs */

//- (id)init
//{
//    if (self = [super init]) {
//        self.m_sampleRate = (double)DEFAULT_SAMPLE_RATE;
//        g_num_dislpay_bins = round( (2.0/self.m_sampleRate) * (double)MAX_DISPLAY_FREQ * (double)DISPLAYWIDTH );
//        NSLog(@"# display bins: %d",(int)g_num_dislpay_bins);
//    }
//    return self;
//}

/////////////////////////////////////////////////////////////////
// Called when the view controller's view is loaded
// Perform initialization before the view is asked to draw
- (void)viewDidLoad
{
   [super viewDidLoad];
   
    NSLog(@"lpc VIEW CONTROLLER ACTIVATED");
    
    // GEt our app delegate
    lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    

    // initialize target formant frequencies
    for (int i=0; i<MAX_NUM_TARG_FORMANTS; i++) {
        m_targetFormantFreqs[i] = 0.0;
    }
    m_targFormantIdx = 0;
    
    // Verify the type of view created automatically by the
   // Interface Builder storyboard
   GLKView *view = (GLKView *)self.view;
   NSAssert([view isKindOfClass:[GLKView class]],
      @"View controller's view is not a GLKView");
   
   // Create an OpenGL ES 2.0 context and provide it to the
   // view
   view.context = [[EAGLContext alloc] 
      initWithAPI:kEAGLRenderingAPIOpenGLES2];
   
   // Make the new context current
   [EAGLContext setCurrentContext:view.context];
   
//   self.preferredFramesPerSecond = 32;
    self.preferredFramesPerSecond = 16;
    
    // Create a base effect that provides standard OpenGL ES 2.0
    // Shading Language programs and set constants to be used for 
    // all subsequent rendering
    self.baseEffect = [[GLKBaseEffect alloc] init];
    
    self.baseEffect.useConstantColor = GL_FALSE;
    //self.baseEffect.useConstantColor = GL_TRUE;
    //self.baseEffect.constantColor = GLKVector4Make(1.0f, 1.0f, 1.0f, 1.0f);

    self.baseEffect.light0.enabled = GL_FALSE;

    
    [[appDelegate getAudioGraphController] startGraph];
    
    // OLD WAY
    
    
    // get the audio manager
    //lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    audioManager = (AudioManager *)[appDelegate getAudioManager];
    // ^
    
    // get the AUDInput object
    audInput = appDelegate.m_AudioInput;
    
    // Make audio manager:
    // this comes from AudioInput object
    
    
    // make display manager
    //displayManager = new LPCDisplayManager(audioManager->m_lpc_magSpecResolution, audioManager->m_sampleRate);
    displayManager = new LPCDisplayManager(NUM_LPC_DISPLAY_BINS, audioManager->m_sampleRate);
    // there's a problem with OpenGL and dynamically allocated vertex buffers, so we can't do it that way.
    // But we have to make sure that the LPC magnitude and display resolution are the same, so...
    assert(NUM_LPC_DISPLAY_BINS == audioManager->m_lpc_magSpecResolution);
    
    
    // NEW WAY
    /*
    AUDGraphController *audioGraphController = [[AUDGraphController alloc] init];
    [audioGraphController basicSetupWithPreferredSampleRate:(double)PREF_SAMPLE_RATE];
    
    AUDInput *audInput = [[AUDInput alloc] init];
    [audInput addToGraph:audioGraphController AtMixerIndex:0];
    
    [audioGraphController startGraph];
    
    audioManager = (AudioManager *)[audInput getAudioManager];
    
    // make display manager
    //displayManager = new LPCDisplayManager(audioManager->m_lpc_magSpecResolution);
    displayManager = new LPCDisplayManager(NUM_LPC_DISPLAY_BINS);
    */
    
    
    
    // Set the background color stored in the current context
    glClearColor(0.625f, 0.0f, 1.0f, 1.0f); // background color

#ifdef USE_LOCAL_FREQ_VERTS
    //for (int i=0; i<audioManager->m_lpc_magSpecResolution; i++) {
    for (int i=0; i<NUM_LPC_DISPLAY_BINS; i++) {
        _freqVertices[i].positionCoords.x = 0.0;
        _freqVertices[i].positionCoords.y = 0.0;
        _freqVertices[i].positionCoords.z = 0.0;
    }
#endif
    
//
//    // Generate, bind, and initialize contents of a buffer to be
//    // stored in GPU memory
//    glGenBuffers(1,                // STEP 1
//                 &vertexBufferID);
//    glBindBuffer(GL_ARRAY_BUFFER,  // STEP 2
//                 vertexBufferID);
    
}


/////////////////////////////////////////////////////////////////
// GLKView delegate method: Called by the view controller's view
// whenever Cocoa Touch asks the view controller's view to
// draw itself. (In this case, render into a frame buffer that
// shares memory with a Core Animation Layer)
- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    if (!audioManager->canComputeLPC()) {
        return;
    }
    
    
    // Get the LPC data
    int lpcSize = audioManager->m_lpc_magSpecResolution;  //audioManager->m_lpc_BufferSize;
    Float32 lpc_mag_buffer[lpcSize];
        
    /****** acquire lock ******/
    g_opengl_mutex.acquire();

    memcpy(lpc_mag_buffer, audioManager->m_lpc_mag_buffer, lpcSize * sizeof(Float32));
    
    // ****** release lock ******
    g_opengl_mutex.release();

   [self.baseEffect prepareToDraw];

    // set scaling so that specified maximum frequency is displayed
    float normFreq = displayManager->getNormalizedFreq(MAX_DISPLAY_FREQ);
    float scaleX = 1.0 / normFreq;
    
    // this can be used to adjust y-scaling (e.g. to use more range along the y-axis, set a number > 1.0)
    float scaleY = 1.0;
    
    
    /* Strange.
     These two code snippets are for detecting the resence of retina displays
     They seem to give contradictory results.
     
     Only the second one appears to be accurate.
     
    
     // From: http://stackoverflow.com/questions/25993446/how-to-detect-retina-hd-display
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 3.0)
        NSLog(@"Retina HD");
    else
        NSLog(@"Non Retina HD");
    
     // From: http://stackoverflow.com/questions/3504173/detect-retina-display
     
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        NSLog(@"Also Retina");
    } else {
        NSLog(@"Also non-retina");
    }
     
     */
    
    // Note: All the devices we've been testing appear to have retina displays
    
    // For non-retina displays:
    //glViewport(rect.origin.x, rect.origin.y, (scaleX*rect.size.width) * 1, (scaleY*rect.size.height) * 1);
    
    // For retina displays:
    //glViewport(rect.origin.x, rect.origin.y, (scaleX*rect.size.width) * 2, (scaleY*rect.size.height) * 2);

    // This of course could be made far more efficient and clean...
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0)) {
        glViewport(rect.origin.x, rect.origin.y, (scaleX*rect.size.width) * 2, (scaleY*rect.size.height) * 2);
    } else {
        glViewport(rect.origin.x, rect.origin.y, (scaleX*rect.size.width) * 1, (scaleY*rect.size.height) * 1);
    }


#ifdef USE_LOCAL_FREQ_VERTS
    displayManager->render(lpc_mag_buffer,_freqVertices, _peakVertices);
    displayManager->renderTargetFormantFreqs(_targetFreqVertices, m_targetFormantFreqs, MAX_NUM_TARG_FORMANTS);
#else
    displayManager->render(lpc_mag_buffer);
#endif
    //glClearColor(0.625, 0.0, 1.0, 1.0);
    glClearColor(0.0, 0.0, 0.0, 1.0);  // #ts1999 changed to black for outdoor research expo
    glClear(GL_COLOR_BUFFER_BIT);


/******* Not sure this is needed *******
#ifdef USE_LOCAL_FREQ_VERTS
    glBufferData(GL_ARRAY_BUFFER, sizeof(_freqVertices),_freqVertices, GL_DYNAMIC_DRAW);
#else
    glBufferData(GL_ARRAY_BUFFER, sizeof(displayManager->_freqVertices),displayManager->_freqVertices, GL_DYNAMIC_DRAW);
#endif
    glBindBuffer(GL_ARRAY_BUFFER, displayManager->_vertexBufferID);
*/

    // Set color
    for(int i = 0; i < NUM_LPC_DISPLAY_BINS; i++) {
        _freqVertices[i].sceneColor = GLKVector4Make(0.0f, 1.0f, 0.0f, 1.0f);
    }
    
    // draw LPC curve
    glBufferData(GL_ARRAY_BUFFER, sizeof(_freqVertices), _freqVertices, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(SceneVertex), NULL);
    glDrawArrays(GL_LINE_STRIP, 0, NUM_LPC_DISPLAY_BINS);

    // draw peak indicator lines
    
    // Set color of first three verticies
    //// Do this inside render() instead
    for(int i = 0; i < NUM_LPC_DISPLAY_BINS * 2; i++) {
        _peakVertices[i].sceneColor = GLKVector4Make(0.0f, 0.0f, 1.0f, 1.0f);
    }
    
    glBufferData(GL_ARRAY_BUFFER, sizeof(_peakVertices), _peakVertices, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(GLKVertexAttribPosition);
    glVertexAttribPointer(GLKVertexAttribColor, 4, GL_FLOAT, GL_FALSE, sizeof(SceneVertex),
                          (const GLvoid *) offsetof(SceneVertex, sceneColor));
    glEnableVertexAttribArray(GLKVertexAttribColor);
    glDrawArrays(GL_LINES, 0, NUM_LPC_DISPLAY_BINS * 2); // Use three vertices from currently bound buffer
    glDisableVertexAttribArray(GLKVertexAttribPosition);
    
    // draw Snapshot curve
    // ----
    
    // Set color
    for(int i = 0; i < NUM_LPC_DISPLAY_BINS; i++) {
        _snapshotVerticies[i].sceneColor = GLKVector4Make(1.0f, 1.0f, 0.0f, 1.0f);
    }
    
    glBufferData(                  // STEP 3
                 GL_ARRAY_BUFFER,  // Initialize buffer contents
                 sizeof(_snapshotVerticies), // Number of bytes to copy
                 _snapshotVerticies,         // Address of bytes to copy
                 GL_DYNAMIC_DRAW);  // Hint: cache in GPU memory
    
    
    // Enable use of positions from bound vertex buffer
    glEnableVertexAttribArray(      // STEP 4
                              GLKVertexAttribPosition);
    
    glVertexAttribPointer(          // STEP 5
                          GLKVertexAttribPosition,
                          3,                   // three components per vertex
                          GL_FLOAT,            // data is floating point
                          GL_FALSE,            // no fixed point scaling
                          sizeof(SceneVertex), // no gaps in data
                          NULL);               // NULL tells GPU to start at
    
    // Set color
    glEnableVertexAttribArray(GLKVertexAttribColor);
    glVertexAttribPointer(
                          GLKVertexAttribColor,
                          4,
                          GL_FLOAT,
                          GL_FALSE,
                          sizeof(SceneVertex),
                          (const GLvoid *) offsetof(SceneVertex, sceneColor));
    
    glDrawArrays( GL_LINE_STRIP,      // STEP 6
                 0,  // Start with first vertex in currently bound buffer
                 NUM_LPC_DISPLAY_BINS); //lpcSize); //DISPLAYWIDTH); // Use three vertices from currently bound buffer
    
    // draw lines for any target formant frequencies
    glBufferData(                  // STEP 3
                 GL_ARRAY_BUFFER,  // Initialize buffer contents
                 sizeof(_targetFreqVertices), // Number of bytes to copy
                 _targetFreqVertices,         // Address of bytes to copy
                 GL_DYNAMIC_DRAW);  // Hint: cache in GPU memory
    
    // Enable use of positions from bound vertex buffer
    glEnableVertexAttribArray(      // STEP 4
                              GLKVertexAttribPosition);
    
    glVertexAttribPointer(          // STEP 5
                          GLKVertexAttribPosition,
                          3,                   // three components per vertex
                          GL_FLOAT,            // data is floating point
                          GL_FALSE,            // no fixed point scaling
                          sizeof(SceneVertex), // no gaps in data
                          NULL);               // NULL tells GPU to start at
    // beginning of bound buffer
    
    glDrawArrays( GL_LINES,      // STEP 6
                 0,  // Start with first vertex in currently bound buffer
                 2*MAX_NUM_TARG_FORMANTS); //lpcSize); //DISPLAYWIDTH); // Use three vertices from currently bound buffer
    

    
}


/////////////////////////////////////////////////////////////////
// Called when the view controller's view has been unloaded
// Perform clean-up that is possible when you know the view 
// controller's view won't be asked to draw again soon.
- (void)viewDidUnload
{
   [super viewDidUnload];
   
    //delete historyDblBuffer;
    
    delete displayManager;
    
   // Make the view's context current
   GLKView *view = (GLKView *)self.view;
   [EAGLContext setCurrentContext:view.context];
    
//   // Delete buffers that aren't needed when view is unloaded
//   if (0 != vertexBufferID)
//   {
//      glDeleteBuffers (1,          // STEP 7 
//                       &vertexBufferID);  
//      vertexBufferID = 0;
//   }
   
   // Stop using the context created in -viewDidLoad
   ((GLKView *)self.view).context = nil;
   [EAGLContext setCurrentContext:nil];
}



/////////////////////////////////////////////////////////////////
// GUI stuff
/////////////////////////////////////////////////////////////////

- (void) viewDidAppear:(BOOL) animated {
    // -- For global vars
    lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // Trying to set a control as visible.
    //_compressThrLabel.hidden = false;
    
    [super viewDidAppear: true];
    
    
    
}

- (IBAction)lcpOrderChanged:(UISlider *)sender
{
    int lpcOrder = roundf(sender.value);
//    NSLog(@"changing LPC order to %d",lpcOrder);
    
    audioManager->disable_lpc_compute();
    audioManager->setLPCOrder((UInt32)lpcOrder);
    audioManager->enable_lpc_compute();
    
    self.lpcOrderLabel.text = [NSString stringWithFormat:@"%d",lpcOrder];
    [self.lpcOrderSlider setValue:lpcOrder animated:YES];
}

- (void)displaySceneVertex:(SceneVertex*) s length:(int)l {
    NSLog(@"After getting passed size: %i at %p", sizeof(s), s);
    NSLog(@"SceneVertex dump:");
    NSLog(@"Size of first element is: %i", sizeof(s));
    NSLog(@"Number of elements is: %i", l);
    //NSLog(@"%@", s[0]);
    for(int i = 0; i < l; i++) {
        NSLog(@"Position at element %i is %f and %f", i, s[i].positionCoords.x, s[i].positionCoords.y);
    }
}

-(void)enterTargetFormantFreq:(double)freq {

    NSLog(@"adding target formant frequency %f",freq);
    
    m_targetFormantFreqs[m_targFormantIdx] = freq;
    
    m_targFormantIdx++;
    if (m_targFormantIdx >= MAX_NUM_TARG_FORMANTS) {
        m_targFormantIdx = 0;
    }
}

- (void)clearAllTargetFormantFreqs {
    
    NSLog(@"Clearing all target formants");
    
    memset(m_targetFormantFreqs, 0, MAX_NUM_TARG_FORMANTS*sizeof(double));
    
    // Why is this not working?
    for (int i=0; i <= MAX_NUM_TARG_FORMANTS; i++) {
        m_targetFormantFreqs[i] = 0.0;
    }
}

-(void) setFilterOrder: (int)filterOrder {
    
    audioManager->disable_lpc_compute();
    audioManager->setLPCOrder((UInt32)filterOrder);
    //audioManager->setLPCOrder((UInt32)lpcOrder);
    audioManager->enable_lpc_compute();
}

- (int)filterOrder
{
    return audioManager->m_lpc_order;
}

- (BOOL)isRecording
{
    return audInput.isRecording;
}

- (void)startRecordingWithSession:(LPCRecordingSession *)session username:(NSString *)username
{
    if (!self.isRecording) {
        [self willChangeValueForKey:@"isRecording"];
        LPCRecordingSessionData data = [session dataWithLpcOrder:audioManager->m_lpc_order];
        [audInput startRecordingWithSessionData:&data];
        [self didChangeValueForKey:@"isRecording"];
    }
}

- (void)stopRecording
{
    if (self.isRecording) {
        [self willChangeValueForKey:@"isRecording"];
        [audInput stopRecording];
        [self didChangeValueForKey:@"isRecording"];
    }
}

@end