//
//  SettingsController.m
//  lpc_app
//
//  Created by moonbot on 8/27/14.
//
//

#import "SettingsController.h"

#import "AUDCustomSegue.h"
#import "lpc_appViewController.h"
#import "lpc_appAppDelegate.h"

@interface SettingsController ()

@end

@implementation SettingsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier {
    
    NSLog((@"Unwinding from segue"));
    return [[AUDCustomSegue alloc] initWithIdentifier:@"FilterOrderSegue" source:NULL destination: self];
}
*/

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"Preparing for push segue");
    
    // This isn't what you expect it is.
    // Does not appear to reference actual view controller
    //lpc_appViewController *d = segue.destinationViewController;
    //d.captureMode = @"filterOrder";
    
}

/// Possible race condtion here, where it might not set mode in time...
-(void) filterOrderButton:(id)sender {
    NSLog(@"Filterorderbutton");
    //lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.captureMode = @"filterOrder";
}

-(void) captureButton:(id)sender {
    NSLog(@"capturebutton");
    //takeSnapshot
    lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.captureMode = @"takeSnapshot";
    NSLog(appDelegate.captureMode );
}

-(IBAction) tempPracticeButton:(id)sender {
    lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.captureMode = @"";
}

-(IBAction) settingButton:(id)sender {
	UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"Multiple templates not yet implemented" message:@""
                          delegate:self cancelButtonTitle:@"Ok"
                          otherButtonTitles: nil,
                          nil];
	[alert show];
	//[alert release];
}

/*
-(void) captureMode:(id)sender {
    NSLog(@"Setting capture mode...");
}
 */

@end
