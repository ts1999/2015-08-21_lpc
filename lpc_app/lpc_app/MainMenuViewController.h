//
//  MainMenuViewController.h
//  lpc_app
//
//  Created by moonbot on 9/11/14.
//
//

#import <UIKit/UIKit.h>
#import "lpc_appViewController.h"
#import "lpc_appAppDelegate.h"


@interface MainMenuViewController : UIViewController

- (IBAction) practiceButton:(id)sender;

@end

