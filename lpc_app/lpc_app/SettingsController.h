//
//  SettingsController.h
//  lpc_app
//
//  Created by moonbot on 8/27/14.
//
//

#import <UIKit/UIKit.h>

#import "AUDCustomSegue.h"
#import "lpc_appViewController.h"
#import "lpc_appAppDelegate.h"

//GLKViewController
//@interface SettingsController : GLKViewController
@interface SettingsController : UIViewController

-(IBAction) captureButton:(id)sender;
-(IBAction) filterOrderButton:(id)sender;
-(IBAction) settingButton:(id)sender;
// This is TEMPORARY, and should probably be in it's own view controller...
-(IBAction) tempPracticeButton:(id)sender;


@end
