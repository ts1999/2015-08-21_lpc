//
//  lpc_appViewController.h
//  lpc_app
//

#import <GLKit/GLKit.h>

#include "LPCDisplayManager.h"

@class AUDGraphController;
@class LPCRecordingSession;

@interface lpc_appViewController : GLKViewController
{
   GLuint vertexBufferID;		/**< OpenGL vertex buffer ID */
}

@property (strong, nonatomic) GLKBaseEffect *baseEffect;					/**< OpenGL base effect, used to set OpenGL properties */

@property (nonatomic) double m_sampleRate;									/**< audio sample rate */

@property (weak, nonatomic) IBOutlet UILabel *compressThrLabel;				/**< unused */
@property (weak, nonatomic) IBOutlet UILabel *expanderThrLabel;				/**< unused */
@property (weak, nonatomic) IBOutlet UILabel *lpcOrderLabel;				/**< GUI label for widget to set number LPC coefficients */
@property (weak, nonatomic) IBOutlet UISlider *lpcOrderSlider;				/**< GUI widget to set number LPC coefficients */
@property (copy, nonatomic) NSString* captureMode;							/**< unused? */
@property (nonatomic, readonly) BOOL isRecording;
//@property (weak, nonatomic) IBOutlet UIButton *captureButton;

/**
 * Callback method for GUI slider widget to set LPC order 
 * @param[in] sender GUI slider object
 */
- (IBAction)lcpOrderChanged:(UISlider *)sender;

- (IBAction)didPressCaptureButton:(id)sender;

- (IBAction)didPressBackButton:(id)sender;

/**
 * Callback method for enter target formant frequency widget 
 * @param[in] freq target formant frequency (Hz)
 */
- (void)enterTargetFormantFreq:(double)freq;


/**
 * Callback method for clear all target formant frequencies
 */
- (void)clearAllTargetFormantFreqs;

- (SceneVertex)getFreqVeritcies;											/**< unused */
- (void)displaySceneVertex:(SceneVertex*) s length:(int)l;

- (IBAction)unwindAction:(UIStoryboardSegue *)segue;

/**
 * Set the LPC filter order (number of LPC filter coefficients) in AudioManager instance
 * @param[in] filterOrder LPC filter order (number LPC coefficients)
 */
-(void) setFilterOrder: (int)filterOrder;
- (int) filterOrder;

-(void)startRecordingWithSession:(LPCRecordingSession *)session username:(NSString *)username;
-(void)stopRecording;

@end
