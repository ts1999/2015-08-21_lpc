//
//  ViewController.m
//  lpc_app
//
//  Created by moonbot on 1/29/15.
//
//

#import "ViewController.h"

#import <MessageUI/MessageUI.h>

#import "LPCAccountManager.h"
#import "lpc_appViewController.h"
#import "LPCRecordingSession.h"
#import "LPCWordPracticeSession.h"
#import "NSURL+QueryStringParams.h"

#define ViewController_MAX_WORD_PRACTICE_COUNT      60

static void *s_recordingStateContext = &s_recordingStateContext;

@interface ViewController () <UIWebViewDelegate, MFMailComposeViewControllerDelegate>

@property (nonatomic, strong) NSString *candidateUsername;
@property (strong, nonatomic) IBOutlet UIWebView *webview;

@end

@implementation ViewController

lpc_appViewController *v;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Minimal, simple way.
    // Next, try making it a property.
    v = [[lpc_appViewController alloc] init];
    v.view.layer.frame = CGRectMake(100, 100, 200, 200);
    //v.view.layer.zPosition = 0;
    v.view.layer.zPosition = -5;
    
    v.view.hidden = false;
    
    [self addChildViewController:v];
    [self.view addSubview:v.view];
    [v didMoveToParentViewController:self];
    
    /*
     GLKViewControllerContainer *g = [[GLKViewControllerContainer alloc] init];
     g.view.layer.frame = CGRectMake(10, 50, 200, 200);
     g.view.layer.zPosition = 1;
     
     [self addChildViewController: g];
     [self.view addSubview:g.view];
     [g didMoveToParentViewController:self];
     */
    
    self.webview.delegate = self;
    //_webview.delegate = self;
    
    
    _webview.scrollView.scrollEnabled = NO;
    _webview.scrollView.bounces = NO;
    
    
    /*
     webView.opaque = NO;
     webView.backgroundColor = [UIColor clearColor];
     */
    _webview.opaque = NO;
    _webview.backgroundColor = [UIColor clearColor];
    
    
    //_url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"]];
    
    //[self.webview loadRequest:[NSURLRequest requestWithURL:_url]];
    
    //NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"]];
    //[self.webview loadRequest:[NSURLRequest requestWithURL:url]];
    
    // Disable network activity
    //[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    [self setUrl:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"]]];
    
    [self loadWebView:self];
    [self attachNotifications];
}

- (void) attachNotifications //#st
{
    [v addObserver:self forKeyPath:@"isRecording" options:0 context:s_recordingStateContext];
}

// The purpose of this is to set the url from the sending segue referring to this.
- (void) loadWebView: (id)sender {
    if(_url != NULL) {
        //NSLog(@"url property set to %@", _url);
        
        
        [self.webview loadRequest:[NSURLRequest requestWithURL:_url]];
    } else {
        NSLog(@"Warning, url property not set\n");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)didPressLPCToggle:(id)sender { //#st
    v.view.hidden = !v.view.hidden;
}

- (IBAction)didPressLPCRMoveRight:(id)sender {  //#st
    [v.view setFrame:CGRectMake(v.view.frame.origin.x + 5,
                                v.view.frame.origin.y,
                                v.view.frame.size.width,
                                v.view.frame.size.height)];
}

- (NSInteger)indexOfAccountWithUUID:(NSString *)uuid
{
    LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
    return [[accountManager userAccounts] indexOfObjectIdenticalTo:[accountManager userAccountWithUUID:uuid]];
}

- (NSString *)jsonAccountsList
{
    LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
    NSMutableArray *accounts = [NSMutableArray array];
    for (LPCAccount *account in [accountManager userAccounts]) {
        [accounts addObject:@{@"uuid":account.uuid, LPCAccountKeyName:account.name}];
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:accounts options:0 error:nil];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (NSString *)jsonForAccount:(LPCAccount *)account
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:account.metadata];
    [dict setObject:account.uuid forKey:@"uuid"];
    [dict setObject:@([self indexOfAccountWithUUID:account.uuid]) forKey:@"index"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

- (LPCAccount *) createAccount
{
    LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
    return [accountManager createUserAccount];
}

- (void)clearUsernames
{
    LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
    [accountManager deleteAllUserAccounts];
}

// This should upload the files to a remote server or something, but for now we'll just send an email
- (void)uploadSession:(id<LPCUploadable>)uploadable
{
    // Email Subject
    NSString *emailTitle = @"Test Email";
    // Email Content
    NSString *messageBody = [uploadable title];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    
    for (int i=0; i<[uploadable numberOfAttachments]; i++) {
        [mc addAttachmentData:[uploadable dataForAttachmentAtIndex:i]
                     mimeType:[uploadable mimeTypeForAttachmentAtIndex:i]
                     fileName:[uploadable nameForAttachmentAtIndex:i]];
    }
    
    // Present mail view controller on screen
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
        NSLog(@"Mail cancelled");
        break;
        case MFMailComposeResultSaved:
        NSLog(@"Mail saved");
        break;
        case MFMailComposeResultSent:
        NSLog(@"Mail sent");
        break;
        case MFMailComposeResultFailed:
        NSLog(@"Mail sent failure: %@", [error localizedDescription]);
        break;
        default:
        break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void) showUsernamePrompt
{
    UIAlertController *pickAccountAlert = [UIAlertController
                                           alertControllerWithTitle:NSLocalizedString(@"Select a Username", nil)
                                           message:NSLocalizedString(@"Please pick a username before you start recording", nil)
                                           preferredStyle:UIAlertControllerStyleAlert];
    [pickAccountAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Okay", nil)
                                                         style:UIAlertActionStyleDefault
                                                       handler:nil]];
    [self presentViewController:pickAccountAlert animated:YES completion:nil];
}

- (void) showUploadPromptForUploadable:(id<LPCUploadable>)uploadable  // #st #hc
{
    UIAlertController *uploadSessionController = [UIAlertController
                                                  alertControllerWithTitle:NSLocalizedString(@"Upload Session", nil)
                                                  message:nil
                                                  preferredStyle:UIAlertControllerStyleAlert];
    [uploadSessionController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Okay", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  [self uploadSession:uploadable];
                                                              }]];
    [uploadSessionController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                style:UIAlertActionStyleCancel
                                                              handler:nil]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:uploadSessionController animated:YES completion:nil];
    });
}

- (void) startRecordingSessionForCurrentAccount // #st #hc
{
    LPCAccountManager *am = [LPCAccountManager sharedInstance];
    LPCAccount *acc = am.currentUserAccount;
    LPCRecordingSession *recSesh = [am beginRecordingSession];
    [v startRecordingWithSession:recSesh username:acc.name];
}

- (void) endWordPracticeSession
{
    LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
    LPCWordPracticeSession *session = accountManager.currentWordPracticeSession;
    if (session) {
        NSString *ratingString = [self.webview stringByEvaluatingJavaScriptFromString:@"getCurrentRating();"];
        NSInteger ratingAsInt = [ratingString integerValue]+1;
        if (ratingAsInt != LPCWordPracticeRatingNone) {
            [session setRatingForCurrentWord:(LPCWordPracticeRating) ratingAsInt];
            [session advance];
        }
        [v stopRecording];
        [accountManager endWordPracticeSession];
        [self.webview stringByEvaluatingJavaScriptFromString:@"endPracticeSession()"];
        [self showUploadPromptForUploadable:session];
    }
}

// Change this later...

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSURL *URL = [request URL];
    //NSLog(@"url detected");
    
    if ([[URL scheme] isEqualToString:@"lpc"]) {
        
        NSLog(@"%@", URL.standardizedURL);

        if([[URL host] isEqualToString:@"setfilterorder"]) {
            NSArray *a = [URL pathComponents];
            
            if(a) {
                int newFilterOrder = [[a lastObject] intValue];
                NSLog(@"Setting filter order to %i", newFilterOrder);
                [v setFilterOrder: newFilterOrder];
            }
        }
        else if ([[URL host] isEqualToString:@"record"]) {
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            if (v.isRecording) {
                LPCRecordingSession *sesh = [accountManager currentRecordingSession];
                [v stopRecording];
                [self showUploadPromptForUploadable:sesh];
            } else {
                if (accountManager.currentUserAccount == nil)
                    [self showUsernamePrompt];
                else
                    [self startRecordingSessionForCurrentAccount];
            }
        }
        
        else if ([[URL host] isEqualToString:@"wordpractice"]) {
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            if (accountManager.currentUserAccount == nil) {
                [self showUsernamePrompt];
            } else {
                if (accountManager.currentWordPracticeSession == nil) {
                    LPCWordPracticeSession *session = [accountManager beginWordPracticeSession];
                    if (session) {
                        [self startRecordingSessionForCurrentAccount];
                        session.recordingSession = accountManager.currentRecordingSession;
                        [webView stringByEvaluatingJavaScriptFromString:@"beginPracticeSession()"];
                        NSString *setWordJS = [NSString stringWithFormat:@"setCurrentPracticeWord('%@', %d, %d)", [session currentWord], session.currentWordIndex+1, ViewController_MAX_WORD_PRACTICE_COUNT];
                        [webView stringByEvaluatingJavaScriptFromString:setWordJS];
                    }
                }
            }
        }
        
        else if ([[URL host] isEqualToString:@"setwordrating"]) {
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            LPCWordPracticeSession *session = accountManager.currentWordPracticeSession;
            if (session) {
                NSInteger rating = [[URL firstParamValueForKey:@"rating"] integerValue]+1;
                [session setRatingForCurrentWord:rating];
            }
            
        }
        
        else if ([[URL host] isEqualToString:@"nextword"]) {
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            LPCWordPracticeSession *session = accountManager.currentWordPracticeSession;
            if (session) {
                [session advance];
                if (session.currentWordIndex >= ViewController_MAX_WORD_PRACTICE_COUNT) {
                    [self endWordPracticeSession];
                } else {
                    NSString *setWordJS = [NSString stringWithFormat:@"setCurrentPracticeWord('%@', %d, %d)", [session currentWord], session.currentWordIndex+1, ViewController_MAX_WORD_PRACTICE_COUNT];
                    [webView stringByEvaluatingJavaScriptFromString:setWordJS];
                }
            }
        }
        
        else if ([[URL host] isEqualToString:@"endpractice"]) {
            [self endWordPracticeSession];
        }
        
        else if ([[URL host] isEqualToString:@"createuser"]) {
            NSString *callbackFnName = [URL firstParamValueForKey:@"callback"];
            LPCAccount *newAccount = [self createAccount];
            NSString *accountJson = [self jsonForAccount:newAccount];
            NSString *javascriptCall = [NSString stringWithFormat:@"%@(%@);", callbackFnName, accountJson];
            [self.webview stringByEvaluatingJavaScriptFromString:javascriptCall];
        }
        
        else if ([[URL host] isEqualToString:@"deleteallusers"]) {
            UIAlertController *areYouSureAlert = [UIAlertController
                                                  alertControllerWithTitle:NSLocalizedString(@"Delete all users", nil)
                                                  message:NSLocalizedString(@"Are you sure? This cannot be undone", nil)
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [areYouSureAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Delete", nil)
                                                                style:UIAlertActionStyleDestructive
                                                              handler:^(UIAlertAction * _Nonnull action) {
                                                                  [self clearUsernames];
                                                              }]];
            [areYouSureAlert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                style:UIAlertActionStyleDefault
                                                              handler:nil]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:areYouSureAlert animated:YES completion:nil];
            });
        }
        
        else if ([[URL host] isEqualToString:@"getselecteduser"]) {
            NSString *callbackFnName = [URL firstParamValueForKey:@"callback"];
            LPCAccount *account = [[LPCAccountManager sharedInstance] currentUserAccount];
            NSString *accountJson = @"{}";
            if (account) {
                accountJson = [self jsonForAccount:account];
            }
            NSString *javascriptCall = [NSString stringWithFormat:@"%@(%@);", callbackFnName, accountJson];
            [self.webview stringByEvaluatingJavaScriptFromString:javascriptCall];
        }
        
        else if ([[URL host] isEqualToString:@"selectuser"]) {
            NSString *accountUUID = [URL firstParamValueForKey:@"uuid"];
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            LPCAccount *account = [accountManager userAccountWithUUID:accountUUID];
            if (account)
                accountManager.currentUserAccount = account;
        }
        
        else if ([[URL host] isEqualToString:@"setuservalue"]) {
            NSString *accountUUID = [URL firstParamValueForKey:@"uuid"];
            NSString *accountKey = [URL firstParamValueForKey:@"key"];
            NSString *accountValue = [URL firstParamValueForKey:@"value"];
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            LPCAccount *account = [accountManager userAccountWithUUID:accountUUID];
            if (account) {
                [accountManager account:account setMetadataValue:accountValue forKey:accountKey];
            }
        }
        
        else if ([[URL host] isEqualToString:@"getuservalue"]) {
            NSString *accountUUID = [URL firstParamValueForKey:@"uuid"];
            NSString *accountKey = [URL firstParamValueForKey:@"key"];
            NSString *callbackFnName = [URL firstParamValueForKey:@"callback"];
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            LPCAccount *account = [accountManager userAccountWithUUID:accountUUID];
            if (account) {
                NSString *value = [account.metadata valueForKey:accountKey];
                NSString *javascriptCall = [NSString stringWithFormat:@"%@(\"%@\", \"%@\", \"%@\");", callbackFnName, accountUUID, accountKey, value];
                [self.webview stringByEvaluatingJavaScriptFromString:javascriptCall];
            }
        }
        
        else if ([[URL host] isEqualToString:@"getuserjson"]) {
            NSString *accountUUID = [URL firstParamValueForKey:@"uuid"];
            NSString *callbackFnName = [URL firstParamValueForKey:@"callback"];
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            LPCAccount *account = [accountManager userAccountWithUUID:accountUUID];
            NSString *accountJson = [self jsonForAccount:account];
            NSString *javascriptCall = [NSString stringWithFormat:@"%@(%@);", callbackFnName, accountJson];
            [self.webview stringByEvaluatingJavaScriptFromString:javascriptCall];
        }
        
        else if ([[URL host] isEqualToString:@"getuserlist"]) {
            NSString *callbackFnName = [URL firstParamValueForKey:@"callback"];
            NSString *jsonAccountList = [self jsonAccountsList];
            NSString *javascriptCall = [NSString stringWithFormat:@"%@(%@);", callbackFnName, jsonAccountList];
            [self.webview stringByEvaluatingJavaScriptFromString:javascriptCall];
        }
        
        else if ([[URL host] isEqualToString:@"getuseratindex"]) {
            NSString *callbackFnName = [URL firstParamValueForKey:@"callback"];
            NSInteger userIndex = [[URL firstParamValueForKey:@"index"] integerValue];
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            NSString *accountJson = [self jsonForAccount:[accountManager.userAccounts objectAtIndex:userIndex]];
            NSString *javascriptCall = [NSString stringWithFormat:@"%@(%@);", callbackFnName, accountJson];
            [self.webview stringByEvaluatingJavaScriptFromString:javascriptCall];
        }
        
        // lpc://getcalculatedvalue?uuid=uuid&value=value&callback=callbackfn
        // kF3Target, kF3TargetStdev and kLPCOrder
        else if ([[URL host] isEqualToString:@"getcalculatedvalue"]) {
            NSString *accountUUID = [URL firstParamValueForKey:@"uuid"];
            NSString *desiredValue = [URL firstParamValueForKey:@"value"];
            NSString *callbackFnName = [URL firstParamValueForKey:@"callback"];
            LPCAccountManager *accountManager = [LPCAccountManager sharedInstance];
            LPCAccount *account = [accountManager userAccountWithUUID:accountUUID];
            NSInteger retVal = -1;
            if (account) {
                if ([desiredValue isEqualToString:@"kF3Target"])
                    retVal = [account targetF3];
                if ([desiredValue isEqualToString:@"kF3TargetStdev"])
                    retVal = [account stdevF3];
                if ([desiredValue isEqualToString:@"kLPCOrder"])
                    retVal = [account targetLPCOrder];
            }
            NSString *javascriptCall = [NSString stringWithFormat:@"%@(%d);", callbackFnName, retVal];
            [self.webview stringByEvaluatingJavaScriptFromString:javascriptCall];
        }
        
        else if([[URL host] isEqualToString:@"toggle"]) {
            NSLog(@"LPC display toggled");
            v.view.hidden = !v.view.hidden;
        }
        else if(([[URL host] isEqualToString:@"setsize"])) {
            //resizeLPC:[URL pathComponents] y:int width:int height:int
            
            //NSLog(@"x: %@ ", [[[URL pathComponents][3]]);
            //[[[URL pathComponents][3]]
            
            //NSString *width = [NSString [URL pathComponents] ];
            
            NSArray *a = [URL pathComponents];
            
            int x = 0;
            int y = 0;
            int width = 0;
            int height = 0;
            
            int i = 0;
            while( (i < ([a count] - 1)) ) {
                
                if([a[i] isEqualToString:@"x"])
                    x = [a[i + 1] intValue];
                if([a[i] isEqualToString:@"y"])
                    y = [a[i+1] intValue];
                if([a[i] isEqualToString:@"height"])
                    height = [a[i+1] intValue];
                if([a[i] isEqualToString:@"width"])
                    width = [a[i+1] intValue];
                
                i++;
            }
            
            //NSLog(@"x: %@ y: %@ width: %@ height: %@", x, y, width, height);
            NSLog(@"x: %i y: %i width: %i height %i", x, y, width, height);
            [self resizeLPC:x y:y width:width height:height];
            
            //NSLog(@"Width: %s", width);
            
        } else if(([[URL host] isEqualToString:@"lpcOverOn"])) {
            // Temporary -- re-evaluate whether we need this feature later
            // Will
            //exit(0);
            NSLog(@"Projecting LPC over");
            v.view.layer.zPosition = 20;
            v.view.hidden = false;
        } else if(([[URL host] isEqualToString:@"lpcOverOff"])) {
            // Temporary -- re-evaluate whether we need this feature later
            // Will
            //exit(0);
            NSLog(@"Projecting LPC over");
            v.view.layer.zPosition = -5;
            v.view.hidden = true;
        }
        else {
            NSLog(@"Ignoring unrecognized host for lpc url scheme: %@", [URL host]);
        }
    }
    
    return true;
}

- (void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if (context == s_recordingStateContext) {
        NSString *recordButtonJS = [NSString stringWithFormat:@"setIsRecording(%d)", (v.isRecording ? 1 : 0)];
        [self.webview stringByEvaluatingJavaScriptFromString:recordButtonJS];
    }
}

- (void) resizeLPC: (int)x y:(int)y width:(int)width height:(int)height {
    NSLog(@"Resizing LPC to (%i, %i) (%i, %i)", x, y, width, height);
    [v.view setFrame:CGRectMake(x, y, width, height)];
}

@end
