//
//  LPCAccountManager.h
//  lpc_app
//
//  Created by Sam Tarakajian on 12/2/15.
//
//

#import <Foundation/Foundation.h>

@class LPCRecordingSession;
@class LPCWordPracticeSession;

NS_ASSUME_NONNULL_BEGIN

FOUNDATION_EXPORT NSString *const LPCAccountKeyName;
FOUNDATION_EXPORT NSString *const LPCAccountKeyAge;
FOUNDATION_EXPORT NSString *const LPCAccountKeyGender;
FOUNDATION_EXPORT NSString *const LPCAccountKeyHeightFeet;
FOUNDATION_EXPORT NSString *const LPCAccountKeyHeightInches;

@interface LPCAccount : NSObject <NSCoding>
@property (nonatomic, readonly) NSString *uuid;
@property (nonatomic, readonly) NSString *name;
@property (nonatomic, readonly) NSDictionary *metadata;
@property (nonatomic, readonly) NSArray<LPCRecordingSession *> *recordings;
@property (nonatomic, readonly) NSArray<LPCWordPracticeSession *> *wordPractices;
@property (nonatomic, readonly) NSInteger targetF3;
@property (nonatomic, readonly) NSInteger stdevF3;
@property (nonatomic, readonly) NSInteger targetLPCOrder;
@end

@interface LPCAccountManager : NSObject
@property (nonatomic, readonly) NSArray<LPCAccount*> *userAccounts;
@property (nonatomic, strong) LPCAccount *currentUserAccount;
@property (nonatomic, readonly) LPCRecordingSession *currentRecordingSession;
@property (nonatomic, readonly) LPCWordPracticeSession *currentWordPracticeSession;
+ (LPCAccountManager *) sharedInstance;

// Accounts
- (LPCAccount *) createUserAccount; // Returns a new user account
- (LPCAccount *) userAccountWithUUID:(NSString *)uuid; // Returns the user account with the given UUID
- (void) deleteUserAccount:(LPCAccount *)account;
- (void) deleteAllUserAccounts;
- (void) account:(LPCAccount *)account setMetadataValue:(nullable id)value forKey:(nonnull NSString *)key;

// Recording Sessions
- (LPCRecordingSession *) beginRecordingSession;
- (LPCRecordingSession *) endRecordingSession;

// Word Practice Sessions
- (LPCWordPracticeSession *) beginWordPracticeSession;
- (LPCWordPracticeSession *) endWordPracticeSession;

NS_ASSUME_NONNULL_END

@end
