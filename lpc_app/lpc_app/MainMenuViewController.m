//
//  MainMenuViewController.m
//  lpc_app
//
//  Created by moonbot on 9/11/14.
//
//

#import "MainMenuViewController.h"
#import "lpc_appViewController.h"
#import "lpc_appAppDelegate.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) practiceButton:(id)sender {
    lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.captureMode = @"";
}

@end
