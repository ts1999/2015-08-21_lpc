//
//  AUDCustomSegue.m
//  AUDLib
//
//  Created by moonbot on 8/27/14.
//  Copyright (c) 2014 Tim Bolstad. All rights reserved.
//


// Make sure to haev this handle all segues

#import "AUDCustomSegue.h"
#import "lpc_appViewController.h"

/*
 Do I want to have the transformation logic done in the custom segue, or in the view controller?
 The advantage of the view controller is that the code to modify itself is in the same file.
 But then again, maybe the code to transform should all be in one place…
 
    Apparently, it's bad design to mess mess with a destination view controller directly.
    Instead, I should set a property, and let it sort itself out.
 
*/

@implementation AUDCustomSegue

-(void) perform {
    UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    UIViewController *destinationController = (UIViewController*)[self destinationViewController];
    
    if([[self identifier]isEqualToString:@"FilterOrderSegue"]) {
        NSLog(@"setting to appropriate mode");
        lpc_appViewController *d = destinationController;
        //d.mode = @"FilterOrder";
        //NSLog(@"Set to #%s", d.mode);
        d.captureMode = @"filterOrder";
        NSLog(@"Set mode to %@", d.captureMode);
    }
    
    
    //_compressThrLabel.hidden = false;
    
    [[self sourceViewController] presentModalViewController:[self destinationViewController] animated:NO];
    
}

@end
