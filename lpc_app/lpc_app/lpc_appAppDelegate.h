/**
 * @file lpc_appDelegate.h
 * @Author Jon Forsyth
 * @brief app delegate
 */

#import <UIKit/UIKit.h>

#import "lpc_appAppDelegate.h"
#import "lpc_appViewController.h"
#import "AUDInput.h"
#import "AUDGraphController.h"

@class AUDInput;

@interface lpc_appAppDelegate : UIResponder <UIApplicationDelegate>

/**
 * Get pointer to instance of AudioManager object 
 * @return pointer to instance of AudioManager object
 */
- (void *)getAudioManager;

/**
 * Get pointer to instance of AUDGraphController object 
 * @return pointer to instance of AUDGraphController object
 */
- (AUDGraphController *)getAudioGraphController;

@property (strong, nonatomic) UIWindow *window;			/**< GUI window */
@property (strong, nonatomic) AUDInput *m_AudioInput;   /**< AUDInput instance */

@property (copy, nonatomic) NSString* captureMode;


@end
