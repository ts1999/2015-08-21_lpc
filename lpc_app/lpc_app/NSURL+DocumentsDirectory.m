//
//  NSURL+DocumentsDirectory.m
//  lpc_app
//
//  Created by Sam Tarakajian on 2/3/16.
//
//

#import "NSURL+DocumentsDirectory.h"

@implementation NSURL (DocumentsDirectory)

+ (instancetype) documentsDirectoryURL
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [NSURL URLWithString:documentsDirectory];
}

@end
