//
//  lpc_appAppDelegate.m
//  lpc_app
//

#import "lpc_appAppDelegate.h"
#import "lpc_appViewController.h"
#import "AUDInput.h"
#import "AUDGraphController.h"

#include "LPCDisplayManager.h"

//#define LPC_DEBUG

#define PREF_SAMPLE_RATE (44100/2)

// For being able to store snapshot array
// Move this onto external object later.
//SceneVertex g_snapshotVerticies[256];

@implementation lpc_appAppDelegate

@synthesize window = _window;

@synthesize captureMode;

AUDGraphController *audioController;

- (void *)getAudioManager
{
    return [self.m_AudioInput getAudioManager];
}

- (AUDGraphController *)getAudioGraphController {
    return audioController;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // OLD WAY
    
    
    // Create audio controller
    // THIS WAS ORIGINALLY A LOCAL VAR
    audioController = [[AUDGraphController alloc] init];
//    [audioController basicSetup];
    [audioController basicSetupWithPreferredSampleRate:(double)PREF_SAMPLE_RATE];
    
    // Add custom audio processing
    self.m_AudioInput = [[AUDInput alloc] init];
    [self.m_AudioInput addToGraph:audioController AtMixerIndex:0];
    
    // Start Audio
    //[audioController startGraph];

    // show some debug information
#ifdef LPC_DEBUG
    NSString *msg = [NSString stringWithFormat:@"sample rate = %f\nbuffer length = %f",audioController.sampleRate, audioController.bufferLengthSec];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"debug info" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
#endif
    
    
    // THIS WAS DISABLED BUT IT STILL FUNCTIONS
    
    // Pass references to view controller.
    //lpc_appViewController *rootVC = (lpc_appViewController*)self.window.rootViewController;
    //rootVC.m_audioGraphController = audioController;
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
   /*
    Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
   /*
    Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
   /*
    Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
   /*
    Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
   /*
    Called when the application is about to terminate.
    Save data if appropriate.
    See also applicationDidEnterBackground:.
    */
}

/*
-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSLog(@"Got url");
    
   // NSArray *d = [URL pathComponents];
    NSArray *d = [url pathComponents];
    NSLog(@"%@", d[0]);
    NSLog(@"I'm exiting!!");
    
    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"From HTML5:" message:(@"%@",d[0]) delegate:self cancelButtonTitle:@"Delete" otherButtonTitles:@"Cancel", nil];
    //[alert show];
    //exit(1);
    return YES;
}
 */

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"Calling Application Bundle ID: %@", sourceApplication);
    NSLog(@"URL scheme:%@", [url scheme]);
    NSLog(@"URL query: %@", [url query]);
    
    
    //exit(1);
    
    lpc_appAppDelegate *appDelegate = (lpc_appAppDelegate *)[[UIApplication sharedApplication] delegate];
    //appDelegate.initialLoad = false;
    
    //NSArray *d = [url pathComponents];
    
    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"From other app" message:(@"Got called externally %@", d[0]) delegate:self cancelButtonTitle:@"Delete" otherButtonTitles:@"Cancel", nil];
    //[alert show];
    
    //takeSnapshot
    
    //if([d[0] isEqualToString:@"capture"]) {
    //    appDelegate.captureMode = @"takeSnapshot";
    //} else {
        //appDelegate.captureMode = @"";
    //}

    //appDelegate.captureMode = d[0];
    //appDelegate.captureMode = @"PracticeWithExit";
    //appDelegate.captureMode = @"takeSnapshowWithExit";
    //[self performSegueWithIdentifier:@"InitialSegue" sender: self];
    
    return YES;
}




@end
