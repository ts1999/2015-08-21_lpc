/**
 * @file AUDInput.h
 * @Author Tim Bolstad, Jon Forsyth
 * @date 12/7/13
 * @brief Copyright (c) 2013-2015 Tim Bolstad. All rights reserved
 */

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "LPCRecordingSessionData.h"
@class AUDGraphController;

@interface AUDInput : NSObject
{
    AudioUnit mInputOutputUnit;						/**< AudioUnit for audio input/output */
    Float32 sinPhase; 								/**< oscillator phase (used for testing only) */
}

@property (nonatomic) UInt32 sampleRate;			/**< audio sample rate */
@property (nonatomic) UInt32 bufferLengthSamples;	/**< length of audio buffer in samples */ 
@property (nonatomic) Float32 frequency;  			/**< oscillator frequency (used for testing only) */
@property (nonatomic, readonly) BOOL isRecording;

/**
 * Add graph controller and mixer to audio graph
 * @param[in] graphController graph controller
 * @param[in] mixerIndex mixer index
 */
- (void)addToGraph:(AUDGraphController*)graphController
      AtMixerIndex:(NSInteger)mixerIndex;

/**
 * Get pointer to instance of AudioManager object 
 * @return pointer to instance of AudioManager object
 */
- (void *)getAudioManager;

- (void)startRecordingWithSessionData:(LPCRecordingSessionData *)sessionData;
- (void)stopRecording;

@end