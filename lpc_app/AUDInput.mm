/**
 * @file AUDInput.mm
 * @Author Tim Bolstad, Jon Forsyth
 * @date 12/7/13 
 * @brief modified version of AUDTestInput.m in AUDLib.
 * @copyright copyright (c) 2013-2015 Tim Bolstad. All rights reserved
 */

#import "AUDInput.h"
#import "AUDGraphController.h"
#import "AUDUtil.h"
#import "AUDFileUtil.h"
#import "LPCAccountManager.h"
#import "LPCRecordingSession.h"
#include "AudioManager.h"
#include "Mutex.h"
#include "LPCSessionRecorder.hpp"

void CAError(OSStatus error, const char *operation)
{
    if (error == noErr) return;
    char errorString[20];
    memset(errorString, 0, 20*sizeof(char));
    // See if it appears to be a 4-char-code
    *(UInt32 *)(errorString + 1) = CFSwapInt32HostToBig(error);
    if (isprint(errorString[1]) && isprint(errorString[2]) &&
        isprint(errorString[3]) && isprint(errorString[4]))
    {
        errorString[0] = errorString[5] = '\'';
        errorString[6] = '\0';
    } else
        // No, format it as an integer
        sprintf(errorString, "%d", (int)error);
    fprintf(stderr, "Error: %s (%s)\n", operation, errorString);
    exit(1);
}

//#define AUDIO_FRAME_SIZE (512)
#define LPC_ORDER (18)                  /**< default number LPC coefficients */
#define LPC_NUM_DISPLAY_BINS (256)      /**< resolution of LPC magnitude spectrum */
//#define TEST_WITH_SIN_WAVE

@interface AUDInput () {
    AudioManager *m_audioManager;           /**< instance of AudioManager object */
    LPCSessionRecorder *m_sessionRecorder;
}
@property (nonatomic, assign) BOOL isRecording;
@end

@implementation AUDInput

- (id)init
{
    if (self = [super init]) {
        NSLog(@"init");
        _frequency = 22050.0 / 100.0;
        sinPhase = 0.0;
    }
    return self;
}

- (void)addToGraph:(AUDGraphController*)graphController
      AtMixerIndex:(NSInteger)mixerIndex
{
    NSLog(@"addToGraph");
    
    AURenderCallbackStruct callbackStruct = [self audioCallback];
    mInputOutputUnit = graphController.ioUnit;
    OSStatus result = [graphController connectAudioCallback:callbackStruct
                                            toMixerInputBus:0];
    if (result) { NSLog(@"***** some kind of error"); }
        
    [graphController updateGraph];

    self.sampleRate = (UInt32) round( (double)graphController.sampleRate);
    self.bufferLengthSamples = (UInt32) round( (double)graphController.bufferLengthSec * (double)graphController.sampleRate );
    
    NSLog(@"sample rate:%d buffer size:%d",(unsigned int)self.sampleRate,(unsigned int)self.bufferLengthSamples);
    
    m_audioManager = new AudioManager(self.bufferLengthSamples, LPC_ORDER, LPC_NUM_DISPLAY_BINS, (Float32)graphController.sampleRate);
    m_audioManager->enable_lpc_compute();
    
    AudioStreamBasicDescription streamAsbd;
    UInt32 size = sizeof(AudioStreamBasicDescription);
    AudioUnitGetProperty(mInputOutputUnit,
                         kAudioUnitProperty_StreamFormat,
                         kAudioUnitScope_Input,
                         0,
                         &streamAsbd,
                         &size);
    streamAsbd.mSampleRate = self.sampleRate;
    
    AudioStreamBasicDescription fileAsbd = streamAsbd;
    fileAsbd.mChannelsPerFrame = 1;
    fileAsbd.mFormatFlags = kAppleLosslessFormatFlag_32BitSourceData;
    fileAsbd.mFormatID = kAudioFormatAppleLossless;
    fileAsbd.mFramesPerPacket = 4096;
    fileAsbd.mBytesPerFrame = fileAsbd.mChannelsPerFrame * (fileAsbd.mBitsPerChannel/8);
    fileAsbd.mBytesPerPacket = fileAsbd.mFramesPerPacket * fileAsbd.mBytesPerFrame;
    
    m_sessionRecorder = new LPCSessionRecorder(&streamAsbd, &fileAsbd);
    
    NSLog(@"lpc mag spec resolution: %d",(unsigned int)m_audioManager->m_lpc_magSpecResolution);
}

- (void *)getAudioManager
{
    return (void *)m_audioManager;
}

- (void)dealloc
{
    delete m_audioManager;
    delete m_sessionRecorder;
}


/** 
 * Callback function to handle device audio input, i.e. LPC coefficients computed on audio buffer.
 */
static OSStatus micInput(void *inRefCon,
                         AudioUnitRenderActionFlags *ioActionFlags,
                              const AudioTimeStamp *inTimeStamp,
                              UInt32 inBusNumber,
                              UInt32 inNumberFrames,
                              AudioBufferList *ioData)
{
	// Get a reference to the object that was passed with the callback
	AUDInput *THIS = (__bridge AUDInput*)inRefCon;
    AudioUnit remoteIO = THIS->mInputOutputUnit;
    
    OSStatus renderErr = noErr;
	UInt32 bus1 = 1; // Bus 1 on remoteIO == mic / audio hardware input
    renderErr = AudioUnitRender(remoteIO,
                                ioActionFlags,
                                inTimeStamp,
                                bus1,
                                inNumberFrames,
                                ioData);
    
    
    Float32 *inA = (Float32 *)ioData->mBuffers[0].mData;
    Float32 *inB = (Float32 *)ioData->mBuffers[1].mData;
    
    // Write the audio asynchronously, if recording
    if (THIS->_isRecording) {
        THIS->m_sessionRecorder->WriteAudio(inNumberFrames, ioData);
    }
    
    //if (THIS->sinPhase == 0.0) printf("%d ",(unsigned int)inNumberFrames);
    
#ifdef TEST_WITH_SIN_WAVE
    Float64 sampleRate = 22050.0; //THIS->_sampleRate;
    Float32 freq = THIS->_frequency;
	// Get a pointer to the dataBuffer of the AudioBufferList
    //Float32 *outA = (Float32 *)ioData->mBuffers[0].mData;
	//Float32 *outB = (Float32 *)ioData->mBuffers[1].mData;
    
	// The amount the phase changes in  single sample
	double phaseIncrement = 2 * M_PI * freq / sampleRate;
	// Pass in a reference to the phase value, you have to keep track of this
	// so that the sin resumes right where the last call left off
	Float32 phase = THIS->sinPhase;
    
	double sinSignal;
	// Loop through the callback buffer, generating samples
	for (UInt32 i = 0; i < inNumberFrames; ++i) {
        // calculate the next sample
        sinSignal = sin(phase);
        inA[i] = sinSignal;
        //outB[i] = sinSignal;
        phase = phase + phaseIncrement;
    }
    // Reset the phase value to prevent the float from overflowing
    if (phase >= 2 * M_PI * freq) {
        phase = phase - 2 * M_PI * freq;
    }
    // Store the phase for the next callback.
    THIS->sinPhase = phase;
    
    memcpy( m_audioManager->m_lpc_mag_buffer, inA, inNumberFrames * sizeof(Float32));

#else
    THIS->m_audioManager->grabAudioData(inA);
    THIS->m_audioManager->computeLPC();
    if (THIS->_isRecording)
        THIS->m_sessionRecorder->WriteLPCCoefficients(inTimeStamp, THIS->m_audioManager->m_lpc_order, THIS->m_audioManager->m_lpc_coeffs);

    memset(inA, 0, inNumberFrames*sizeof(Float32));
    memset(inB, 0, inNumberFrames*sizeof(Float32));
#endif
	return renderErr;
}

/**
 * Bundle up the callback and the reference pointer
 */
- (AURenderCallbackStruct)audioCallback
{
    // Callback Struct
	AURenderCallbackStruct renderCallbackStruct;
    // Function Pointer
	renderCallbackStruct.inputProc = &micInput;
	// Object Reference
    renderCallbackStruct.inputProcRefCon = (__bridge void*)self;
	return renderCallbackStruct;
}

- (void)startRecordingWithSessionData:(LPCRecordingSessionData *)sessionData
{
    if (!self.isRecording) {
        m_sessionRecorder->BeginRecording(sessionData);
        self.isRecording = YES;
    }
}

- (void)stopRecording
{
    if (self.isRecording) {
        m_sessionRecorder->StopRecording();
        self.isRecording = NO;
    }
}

@end
