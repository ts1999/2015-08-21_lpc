function callObjcWithURI(uri) {
    var iframe = document.createElement("IFRAME");
    iframe.setAttribute("src", uri);
    document.documentElement.appendChild(iframe);
    iframe.parentNode.removeChild(iframe);
    iframe = null;
}

function setUsernames(jsonUsernames, selectedName) {
    usernames = JSON.parse(jsonUsernames);
    $("#accounts-group").empty();
    $.each(usernames, function(index, value) {
           elt = document.createElement("option");
           elt.text = value;
           if (value == selectedName)
                elt.selected = 'selected';
           $("#accounts-group").append(elt);
    });
    if (usernames.length == 0) {
        elt = document.createElement("option");
        elt.text = "PSEUDONYM";
        elt.selected = 'selected';
        elt.disabled = 'disabled';
        $("#accounts-group").append(elt);
    }
}

function createNewUsername() {
    callObjcWithURI("lpc://createuser");
    $("#accounts-group:disabled").selected = 'selected';
}

function deleteAllUsers() {
    callObjcWithURI("lpc://deleteallusers");
    $("#accounts-group:disabled").selected = 'selected';
}

function setCurrentUsername(username) {
    callObjcWithURI("lpc://selectuser/" + username);
}

function initializeUsers() {
    callObjcWithURI("lpc://initializeusers");
}

function setIsRecording(isRecording) {
    if (isRecording) {
        $("#record").removeClass("recording").addClass("recording");
        $("#record").text("Stop");
        $("#accounts-picker").attr("disabled", true);
    } else {
        $("#record").removeClass("recording");
        $("#record").text("Record");
        $("#accounts-picker").attr("disabled", false);
    }
}

function toggleLPC() {
    callObjcWithURI("lpc://toggle");
}

function record() {
    callObjcWithURI("lpc://record");
}

function wordpractice() {
    callObjcWithURI("lpc://wordpractice");
}

function beginPracticeSession() {
    $("#textbox").append("<h2 id='wordpractice'></h2>");
    $("#bottomDiv").append("<button id='nextword' class='Btn lpc-button' float='left' onclick='nextWord();'>Next Word</button>");
    $("#bottomDiv").append("<button id='endpractice' class='Btn lpc-button' float='left' onclick='endPracticeSession();'>Finish</button>");
    $("#bottomDiv").append("<form id='rating-form' float='left'>" +
                          "<input type='radio' name='rating' value='low'> Low<br>" +
                          "<input type='radio' name='rating' value='medium'> Medium<br>" +
                          "<input type='radio' name='rating' value='high'> High" +
                          "</form>")
    $("#record").attr("hidden", true);
    $("#wordplay").attr("hidden", true);
}

function endPracticeSession() {
    callObjcWithURI("lpc://endpractice");
    $("#wordpractice").remove();
    $("#rating-form").remove();
    $("#nextword").remove();
    $("#endpractice").remove();
    $("#record").attr("hidden", false);
    $("#wordplay").attr("hidden", false);
}

function getCurrentRating() {
    var radioButtons = $("#rating-form input:radio");
    return radioButtons.index(radioButtons.filter(':checked'));
}

function nextWord() {
    callObjcWithURI("lpc://nextword");
    $("input:radio[name=rating]").prop("checked", false);
}

function setCurrentPracticeWord(word, idx, maxi) {
    $("#wordpractice").html("Word " + idx + " of " + maxi + ": <em>" + word + "</em>");
}

function setLPCSize(x, y, width, height) {
    
    // Placeholder
    // Will at least prevent most error messages in Safari, Chrome ignores schemes it does
    // not recognize. Since much of the web interface is done in a browser,
    // would make sense to have an image placeholder to design around.
    // I have found Safari to better approximate the layout than Chrome.
    
    if(!/iPad|iPhone|iPod/.test(navigator.platform)) {
        //alert("This only works on iOS devices");
        return;
    }
    
	var query = 'lpc://setsize/x/' + x + '/y/' + y + '/width/' + width + '/height/' + height;
    callObjcWithURI(query);
}

function setFilterOrder(filterOrder) {
    callObjcWithURI("lpc://setfilterorder/" + filterOrder);
}
