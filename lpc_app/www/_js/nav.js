/*=====================================================================
  NAVIGATION & PAGE LOADING
    
    The script on this page relates to index.html ONLY, which contains
    the Nav, Header, and iFrame elements only.
    Indiviual app pages are loaded into index.html via the iframe. 
    All page-level init fx are contained within each indiviual source page.

 ===================================================================== */

/* global $, window */

//GLOBALS

var hello = function(){
  alert('Hello!');
}

$(document).ready(function () { 
  "use strict";

  //webViewIndex();

  /* -------------
      SELECT the starting page (see else)
    ---------------------------------------- */
    if (window.location.hash === '#loadProfiles') { loadProfiles();
    } else if (window.location.hash === '#loadTut') { loadTut();
    } else if (window.location.hash === '#loadAuto') { loadAuto();
    } else if (window.location.hash === '#loadFree') { loadFree();
    } else if (window.location.hash === '#loadSyll') { loadSyll();
    } else if (window.location.hash === '#loadWord') { loadWord();
    } else if (window.location.hash === '#loadResources') { loadResources();
    } else // SELECT the starting page for the app by uncommenting a fx
    //loadProfiles();
    //loadTut();
    //loadAuto();
    //loadFree();
    //loadSyll();
    //loadWord();
    loadResources();

  /* -------------
      NAVIGATION
      NO MANUAL SELECTION necessary for this block.
      Script attachs onClick event handlers to all navBtns & calls nav animations & iframe page load on click
    ---------------------------------------- */

  $('#profileNav').on("click", function() {
    if (!$("#profileNav").hasClass("active")){ 
        clearPage();
        loadProfiles();
    }
  });

	$('#tutNav').on("click", function() {
    if(!$("#tutNav").hasClass("active")){ 
      clearPage();
      loadTut();
    }
  });

	$('#autoNav').on("click", function() {
    if(!$("#autoNav").hasClass("active")){ 
      clearPage();
      loadAuto();
    }
  });

	$('#freeNav').on("click", function() { 
    if(!$("#freeNav").hasClass("active")){ 
      clearPage();
      loadFree();
    }
  });

	$('#syllableNav').on("click", function() {
    if(!$("#syllableNav").hasClass("active")){
      clearPage();
      loadSyll();
    }
  });

	$('#wordNav').on("click", function() {
    if(!$("#wordNav").hasClass("active")){ 
      clearPage();
      loadWord();
    } 
  });

	$('#resourcesNav').on("click", function() {
    if(!$("#resourcesNav").hasClass("active")){
      clearPage();
      loadResources();
    }
  });

}); //END anon set-up fx



/*=====================================================================
  HELPER FX TO LOAD AND CLEAR PAGES
  These should only be called from the $(document).ready fx above
 ===================================================================== */

  // Just a helper fx to clear out the nav and ifame attributes 
  var clearPage = function() {
      "use strict";
      
      //clear out Header & iFrame
      $('#pgTitle').html(""); //.css('display', 'none');
      $('iframe').attr('src', '');

      // Update Nav
      $('.navBtn').removeClass('active');
      $("svg").removeAttr("class");
      $("path").removeAttr("class", "bubs"); // OFF state for Syllables & Words 
      $("#star").attr("class", "proWhite"); //OFF state for star icon
      $("#dots").attr("class", "proBlue"); //OFF state for star icon
      $("#negSpace").attr("class", "tutOFF"); //OFF state for tutorial (areas inbetween spokes)
  };


  /* =========== INDIVIDUAL PAGE SET-UP FX FOR INDEX.HTML ========== */
  // Sets '.active' nav btn state and loads each page into the iframe

  function loadProfiles() {
    "use strict";
    $("#profileNav").addClass("active");
    $("#profileSVG").attr("class", "animated tada");
    $("#star").attr("class", "proBlue"); 
    $("#dots").attr("class", "proWhite");

    $('#pgTitle').html("PROFILES");
    $('iframe').attr('src', 'pages/01profiles/01pro_index.html');
  }

  function loadTut() {
    "use strict";
    //nav
    $("#tutNav").addClass("active");
    $("#tutSVG").attr("class", "activeSVG animated rotateIn");
    $("#negSpace").attr("class", "tutON");
    //page
    $('#pgTitle').html("TUTORIAL");
    $('iframe').attr('src', '');
  }

  function loadAuto() {
    "use strict";
    //nav
    $("#autoNav").addClass("active");
    $("#autoSVG").attr("class", "autoON animated flipInX");
    //page
    $('#pgTitle').html("AUTO SESSION");
    $('iframe').attr('src', 'pages/03auto/03auto_index.html');
  }

  var loadFree = function() {
    "use strict";
    //nav
    $("#freeNav").addClass("active");
    $("#freeSVG").attr("class", "freeON animated bounce");
    //page
    $('#pgTitle').html("FREE PLAY");
    $('iframe').attr('src', 'pages/04free/04free_index.html');
    //console.log('free webView is: ' + webView); /* #hc */
  }

  function loadSyll() {
    "use strict";
    //nav
    $("#syllableNav").addClass("active");
    $("#syllableSVG").attr("class", "animated bounceInSyll");
    $("#sylPath").attr("class", "bubs");
      //page
    $('#pgTitle').html("SYLLABLE QUEST");
    $('iframe').attr('src', 'pages/05syllables/05syllables_index.html');
  }

  function loadWord() {
    "use strict";
    //nav
    $("#wordNav").addClass("active");
    $("#wordSVG").attr("class", "activeSVG animated bounceInWord");
    $("#wordPath").attr("class", "bubs");
    //page
    $('#pgTitle').html("WORD CHALLENGE");
    $('iframe').attr('src', 'pages/06words/06words_index.html');
  }

  function loadResources() {
    "use strict";
    //nav
    $("#resourcesNav").addClass("active");
    $("#resourcesSVG").attr("class", "activeSVG animated flipInY");
    //page
    $('#pgTitle').html("SLP RESOURCES");
    $('iframe').attr('src', 'pages/07resources/07resources_index.html');
  }
  
function webViewIndex() {
  $('.indexBody').addClass('webBody');
  $('.indexContainer').addClass('webContainer');
}

