//GLOBALS
var webView = false;
/* ================= LPC SLIDER & CONTROLS ================= */
	
	// plays animation onClick for lpcBtns
	function lpcBtnClick() {
		$(".rate-btn").click(function() {
			$(this).addClass("rate-btn-select", 1, rateBtnDone);
			console.log(this.id);
		});
		$('#target-btn').click(function() {
			targetBtn();
		});

	}
		function rateBtnDone() {
			setTimeout(function() {
				$(".rate-btn").removeClass("rate-btn-select");
			}, 150);
		}
	

	function loadSlider() {
	  $("#F3_slider").slider({
			 min: 0,
			 max: 4500,
			 value: 4500, // starting position
			 change: function(event, ui) {
				$("#sliderLabel").html(ui.value);
	            $("#sliderLabel").css("background", "white").show();
	            if (ui.value < 4499) {
	            	$("#target-btn").html("Clear Target").removeClass("cleared");
	            } else {
	            	$("#target-btn").html("Load Target").attr("class", "lpc-btn cleared");
	            };
			 }, //END label change fx. This displays the new fz value, after a slider position changes. Its necessary to keep displaying the val in the label when the slider is not moving.
			 slide: function(event, ui) {
				$("#sliderLabel").html(ui.value);
				$("#sliderLabel").css("background", "white").show();
	            $('#target-btn').html("Clear Target");
			 } //END slide fx. This displays the fz value, while the slider is moving.
			 
		})
		.draggable(); //END .slider()
		$( "#F3_slider" ).val( $( "#slider-vertical" ).slider( "value" ) );
	    
	     /* The slider widget creates handle elements with the class ui-slider-handle on initialization. You can specify custom handle elements by creating and appending the elements and adding the ui-slider-handle class before initialization. #sliderLabel and .ui-slider-handle are styled in 02_lpc.css */ 
    	$('.ui-slider-handle').addClass('star-handle').append('<span id="sliderLabel"></span>');
	} //END loadSlider()


	function targetBtn() { 	
		if ($("#target-btn").hasClass("cleared")) {  
			var F3 = 1350; //temp value  #st How do we load a saved Target val?
			$("#sliderLabel").show();
			$("#F3_slider").slider("value", F3);
			console.log('f3 target set');
         } else { 
            $("#F3_slider").slider("value", 4500);
			$("#sliderLabel").css("background", "transparent");
			$("#sliderLabel").hide();
			console.log('f3 target cleared'); 
         } 
	} //END targetBtn



/* ================= WEB VIEW ================= */
/*
function webView(webView) {
	$('body').addClass('webBody');
	$('#container').addClass('webContainer');
	webView = true;

	//addWave();
	//placeholderText();
	return	webview;
}

function addWave() {
	$('#lpc').append('<div id="wave"></div>');
}

function placeholderText() {
	$('#word').html('Placeholder');
	$('#progressBar').html('Progress Bar will go here...');
}
*/



/* ================= p5 Wave ================= */
	/* NOTES: re the while() draw loop
		50px for sky
		75px x noise = wave height
	    	Perlin noise ref: http://p5js.org/reference/#p5/noise
	    		Perlin noise is defined in an infinite n-dimensional space where each pair of 
	    		coordinates corresponds to a fixed semi-random value (fixed only for the lifespan 
	    		of the program. p5.js can compute 1D, 2D and 3D noise, depending on the number of 
	    		coordinates given. The resulting value will always be between 0.0 and 1.0. 
	    		The noise value can be animated by moving through the noise space. 
	    		The 2nd and 3rd dimension can also be interpreted as time.

	    	hc note: use the time var and its increments to adjust the personality of the wave.
	*/

var p5wave = function(sketch) {
	var white = sketch.color(255,255,255); //#ffffff
	var sky = sketch.color(197,243,255);  //#C5F3FF;
	var ocean = sketch.color(83,200,233);  //#53C8E9;

	var time = 5;

    sketch.setup = function() {
    	var waveCanvas = sketch.createCanvas(668, 289); 

     	//append the canvas to existing div "wave" in DOM
      	waveCanvas.parent("wave");

     	sketch.frameRate(35);
  	}; //END setup

	sketch.draw = function() {
    	sketch.background(sky);
    	sketch.stroke(ocean);
    	sketch.strokeWeight(4);

	   	var x = 0;

		while(x < sketch.width) {
	    	//syntax:  sketch.line(x1,y1,x2,y2);
	    	sketch.line(x, 69 + 75 * sketch.noise(x/180, time), x, sketch.height);
	    	x += 1;
	   	}

	   time += 0.01;
	}; //END draw
} // p5wave


var loadP5wave = function() {
	var wave = document.getElementById( "wave" );
  	var waveSketch = new p5(p5wave, "wave");
}
/* ================= end ================= */






