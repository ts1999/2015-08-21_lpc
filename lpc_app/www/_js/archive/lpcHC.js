
/* README  [lpc.js]
	qs are tagged by initials (ex. #st or #tb)
 
 */

/* ================= UTILS ================= */

	function callObjcWithURI(uri) {
		var iframe = document.createElement("IFRAME");
		iframe.setAttribute("src", uri);
		document.documentElement.appendChild(iframe);
		iframe.parentNode.removeChild(iframe);
		iframe = null;
	}

/* ================= LOCAL PROFILES ================= */

/*
 * Using the native API for profile storage
 *
 * All profiles will be passed to javascript as JSON objects. Each
 * will have the following fields:
 *  uuid: A unique identifier for the profile
 *  index: The index of the profile relative to the others
 *  name: (optional) A user specified name
 *  age: (optional) A user specified age, in years
 *  gender: (optional) The user's specified gender as a string 'm' or 'f'
 *  heightFeet: (optional) The foot part of the user's height
 *  heightInches: (optional) the inches part of the user's height
 *
 * FUNCTION
 * lpc://createuser?callback=callbackfn
 *      Creates a new user profile. After the profile is created,
 *      calls the callback function callbackfn, passing a JSON
 *      description of the profile as the only argument
 *
 * FUNCTION
 * lpc://getselecteduser?callback=callbackfn
 *      Calls the function named callbackfn with a JSON description of the 
 *      currently selected user profile as the first argument. If no profile
 *      is selected, returns an empty object
 *
 * FUNCTION
 * lpc://selectuser?uuid=uuid
 *      Sets the currently selected profile. uuid is the unique identifier
 *      of the profile to select
 *
 * FUNCTION
 * lpc://setuservalue?uuid=uuid&key=key&value=&value
 *      Sets the value for a specific key for the profile with the given uuid.
 *      Acceptable keys are:
 *          name
 *          age
 *          gender
 *          heightFeet
 *          heightInches
 *
 * FUNCTION
 * lpc://getuservalue?uuid=uuid&key=key&callback=callbackfn
 *      Gets the value for a specific key for the profile with the specified uuid.
 *      The account uuid, key and retrieved value will all be passed to the callback.
 *      So, a candidate callback function might look like this:
 *          function handleValueRequest(uuid, key, value) {
 *              if (key == "gender") {
 *                  // Set the gender
 *              }
 *          }
 *      ATTENTION: The value will be a string, no matter what. Even a call to get a
 *                  numerical value, like age, will return a string value for age,
 *                  which must be converted to a numeral in javascript
 *
 * FUNCTION
 * lpc://getuserjson?uuid=uuid&callback=callbackfn
 *      Gets a description of the user profile with the given uuid as JSON
 *      JSON will be passed to the function named callbackfn
 * 
 * FUNCTION
 * lpc://getuserlist?callback=callbackfn
 *      Calls the function named callbackfn with a JSON representation of the list of users
 *
 * FUNCTION
 * lpc://getuseratindex?index=index&callback=callbackfn
 *      Calles the function named callbackfn with a JSON representation of the user
 *      at index index
 *
 */

function setUsernames(jsonUsernames, selectedName) {
	usernames = JSON.parse(jsonUsernames);
	$("#accounts-group").empty();
	$.each(usernames, function(index, value) {
		   elt = document.createElement("option");
		   elt.text = value;
		   if (value == selectedName)
		   elt.selected = 'selected';
		   $("#accounts-group").append(elt);
		   });
	if (usernames.length == 0) {
		elt = document.createElement("option");
		elt.text = "PSEUDONYM";
		elt.selected = 'selected';
		elt.disabled = 'disabled';
		$("#accounts-group").append(elt);
	}
}

function createNewUsername() {
	callObjcWithURI("lpc://createuser");
	$("#accounts-group:disabled").selected = 'selected';
}

function deleteAllUsers() {
	callObjcWithURI("lpc://deleteallusers");
	$("#accounts-group:disabled").selected = 'selected';
}

function printThreeThings(thing1, thing2, thing3) {
    console.log("thing1: " + thing1);
    console.log("thing2: " + thing2);
    console.log("thing3: " + thing3);
}


/* ================= LPC VIEW ================= */

	function toggleLPC() {
		callObjcWithURI("lpc://toggle");
	}

	function setLPCSize(x, y, width, height) {
		
		// Placeholder
		// Will at least prevent most error messages in Safari, Chrome ignores schemes it does
		// not recognize. Since much of the web interface is done in a browser,
		// would make sense to have an image placeholder to design around.
		// I have found Safari to better approximate the layout than Chrome.
		
		if(!/iPad|iPhone|iPod/.test(navigator.platform)) {
			//alert("This only works on iOS devices");
			return;
		}
		
		var query = 'lpc://setsize/x/' + x + '/y/' + y + '/width/' + width + '/height/' + height;
		callObjcWithURI(query);
	}

/* ================= LPC SLIDER & CONTROLS ================= */
	
	function loadSlider() {
	  $("#F3_slider").slider({
			 min: 0,
			 max: 4500,
			 value: 4500, // starting position
			 change: function(event, ui) {
				$("#sliderLabel").html(ui.value);
	            $("#sliderLabel").css("background", "white").show();
	            $('#target-btn').html("Clear Target") //.removeClass("cleared");
			 }, //END label change fx. This displays the new fz value, after a slider position changes. Its necessary to keep displaying the val in the label when the slider is not moving.
			 slide: function(event, ui) {
				$("#sliderLabel").html(ui.value);
				$("#sliderLabel").css("background", "white").show();
	            $('#target-btn').html("Clear Target");
			 } //END slide fx. This displays the fz value, while the slider is moving.
			 
		})
		.draggable(); //END .slider()
	    
	     /* The slider widget creates handle elements with the class ui-slider-handle on initialization. You can specify custom handle elements by creating and appending the elements and adding the ui-slider-handle class before initialization. #sliderLabel and .ui-slider-handle are styled in 02_lpc.css */ 
    	$('.ui-slider-handle').addClass('star-handle').append('<span id="sliderLabel"></span>');
	} //END loadSlider()


	function targetBtn() {  // #hc need to evaluate based on position & not cleared tags
		if ($("#target-btn").hasClass("cleared")) {  
			$("#target-btn").html("Clear Target").removeClass("cleared");
			var F3 = 1350; //temp value  #st How do we load a saved Target val?
			$("#sliderLabel").show();
			$("#F3_slider").slider("value", F3);
			console.log('f3 target set');
         } else { 
         	$("#target-btn").html("Load Target").attr("class", "lpc-btn cleared");
            $("#F3_slider").slider("value", 4500);
			$("#sliderLabel").css("background", "transparent");
			$("#sliderLabel").hide();
			console.log('f3 target cleared'); 
         } 
	} //END targetBtn


	// plays animation onClick for lpcBtns  #hc Do this better.
	function lpcBtnClick() {
		$(".rate-btn").click(function() {
			$(this).addClass("rate-btn-select", 1, callback);
		});
	}

		function callback() {
			setTimeout(function() {
					   $(".rate-btn").removeClass("rate-btn-select");
					   }, 150);
		}

/* ================= WORD PRACTICE ROUTINE ================= */

// NATIVE INIT: This calls into native code to start the practice session. All init fx should be called here.
function wordpractice() {
	callObjcWithURI("lpc://wordpractice");
}
	//DON'T call this directly--the native code calls this as part of starting the practice session
	function beginPracticeSession() {
		$("#challengeItem").append("<span id='wordpractice'></span>");
		//$('#challengeItem').html('hammer');
	}

	//DON'T call this directly--the native code will call this as it advances from word to word
	function setCurrentPracticeWord(word, idx, maxi) {
		$("#wordpractice").html(word);
		console.log(idx);
		$("#progressBar").html("<span> Progress Bar will go here...  WORD " + idx + "OF " + maxi + "</span>");
	}

	//DON'T call this directly - This calls the native code to advance to the next word
	function nextWord() {
		callObjcWithURI("lpc://nextword");
	}

	//DON'T call this directly - the native end will call this when the word is advanced
	function getCurrentRating(rating) {
		 if (rating === 'low') {
			 callObjcWithURI("lpc://setwordrating?rating=0");
			 console.log('low');
		 } else if (rating === 'medium') {
			 callObjcWithURI("lpc://setwordrating?rating=1");
			 console.log('medium');
		 } else if (rating === 'high') {
			 callObjcWithURI("lpc://setwordrating?rating=2");
			 console.log('high');
		 }
		nextWord();
	}

// This calls the native code to end the session
function endPracticeSession() {
	//
	callObjcWithURI("lpc://endpractice");
	// #hc "Save choices and end session."
	// #tb #st Save what choices? User settings?
	// #tb #st This should check Rec, if rec=true then it shold prompt to Save or Upload, right?
	$("#wordpractice").remove();
	$("#progressBar").html("<span></span>");
	clearTarget();
	//$("#record").attr("hidden", false); //#st
	console.log('session ended');
}


/* ================= RECORDING ================= */

// This calls the native code to begin the recording
function record() {
	callObjcWithURI("lpc://record");
}
/*
function recordBtns() {
	if ($("#record").val() == "false") { 	// if rec is off
		$("#record").val("true").removeClass("recOFF").addClass("recON").text("Stop Record");
		
		// dialog 'Do you want to save the recording to this device? Y/N'
		// #st call a 'start record' fx
		console.log("Rec is now ON");
	} else {  								// if rec is ON
		$("#record").val("false").removeClass("recON").addClass("recOFF").text("Start Record");
		//#hc - dialog 'Do you want to upload the recording for use in research? Y/N
		//#tb #st What happens if they don't upload the sample?
		// #st call an 'end record' fx
		console.log("Rec is now OFF");
	}
}
*/

 function setIsRecording(isRecording) {
	 /*
	 if (isRecording) { // if rec is ON
		//$("#record").removeClass("recording").addClass("recording"); //#st-see new classes above
		 $("#record").removeClass("recON").addClass("recOFF").text("Start Record");
		 //$("#accounts-picker").attr("disabled", true);  //#st #q?
	} else { // if rec is off
		$("#record").removeClass("recOFF").addClass("recON").text("Stop Record");
		//$("#record").removeClass("recording"); //#st-see new classes above
		//$("#record").text("Record");
		//$("#accounts-picker").attr("disabled", false); //#st #q Why is this here? Is it still necessary?
	} */
 }




/* =================
	SETTINGS
 ================= */

function setFilterOrder(filterOrder) {
	callObjcWithURI("lpc://setfilterorder/" + filterOrder);
}
