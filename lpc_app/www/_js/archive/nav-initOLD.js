/*=====================================================================
  PAGE NAVIGATION & LOADING SCRIPTS
    
    OVERVIEW:
      - each nav btn corresponds to a 'page' 
        page = section of the app w/ predifined setup, assets, and methods

 ===================================================================== */

/* global $, window */

//GLOBALS
//var row = "height: 53px;"

var filterOrder;




$(document).ready(function () { 
  "use strict";

  /* ----------------------------------------------------------------------
    CONTAINS SET-UP FX 

      VIEW
      
      START PAGE

      NAVIGATION & 
        Attaches onclick event handlers to each of the Nav btns. 
        If the clicked nav btn is does NOT correspond to the currently '.active' page, 
        then the init fx is called for the new (clicked) page.
  ---------------------------------------------------------------------- */

  //turns on or off WebView
  //webView();

  // Select the starting page
  profilesInit();
  //wordInit(); 
  //freeInit();

  // NAVIGATION
  $('#profileNav').on("click", function() {
    if (!$("#profileNav").hasClass("active")){ 
      profilesInit();
    } // end IF 
  });

	$('#tutNav').on("click", function() {
    if(!$("#tutNav").hasClass("active")){ 
      tutorialInit();
    } // end IF
  });

	$('#autoNav').on("click", function() {
    if(!$("#autoNav").hasClass("active")){ 
      autoInit();
    } // end IF
  });

	$('#freeNav').on("click", function() { 
    if(!$("#freeNav").hasClass("active")){ 
          freeInit();
      } // end IF
  });

	$('#syllableNav').on("click", function() {
    if(!$("#syllableNav").hasClass("active")){
      syllInit();
    } // end IF 
  });

	$('#wordNav').on("click", function() {
    if(!$("#wordNav").hasClass("active")){ 
      wordInit(); 
    } 
  });

	$('#resourcesNav').on("click", function() {
    if(!$("#resourcesNav").hasClass("active")){
        resourcesInit();
    } // end IF 
  });
})


/* =====================================================================
    PAGE SETUP HELPER FXs
   ===================================================================== */

    /* ----------------------------------------------------------------------
      NOTES on clearPage()
        Trigger: Only called from w/in page init fx.
        Purpose: Clears nav states, headers, and text prior it loading the next page's content.
        - the ".css('display', 'none')" methods are necessary for the 'fade in' effects to work on page init
        - svg: removeClass() & addClass(); don't reach the SVGs. YOu have to do: '.attr("class", "proWhite"); //OFF star icon'

    ---------------------------------------------------------------------- */

  function clearPage() {
    "use strict";

      // Update Nav
        $('.navBtn').removeClass('active');
       // $("svg").removeAttr("class", "activeSVG animated tada bounceInWord rotateIn flipInY autoON flipInX");
        $("svg").removeClass(); //several need "active SVG", so this doesn't work yet "  //#hc
          // clean: pro, tut, auto, 
        $("path").removeAttr("class", "bubs");
        $("#star").attr("class", "proWhite"); //OFF star icon
        $("#dots").attr("class", "proBlue"); //OFF star icon
        $("#negSpace").attr("class", "tutOFF"); //OFF tutorial (areas inbetween spokes)

      // Page Header
        $('#pageHeader').html(""); //.css('display', 'none');
        //$('#spacerTop').css(); //adjust spacing here
        //REMOVED  $('#textbox').html(""); // clear out text box spacer

      //LPC-Specific Content
        $('#challengeItem').html(''); // this should go in kill LPC

      //MidBlock Content
        $("#midBlock").children().hide(); //remove(); // remove() and replaceWith() will clear event handlers
  }


  // Evaluates and inits lpc fx common to Word & Syllable pages. c/b = #lpcBlock.loaded 
  function lpcLoad() {
    if (!$("#lpcBlock").hasClass("loaded")) { //if #lpcBlock is NOT loaded
      setLPCSize(170, 334, 680, 310);

      $('#textbox').css("display","none");
      $("#midBlock").css("display","none");

      $("#lpcBlock").addClass("loaded").css("display","block");
      $("#lpcHead, #prog, #rateBox").show().css("display","block");
      
      loadSlider(); //located in lpc.js  

      $('.lpc-btn').fadeIn(600);
      $('#sess-btn').html('End Session'); // event handler for dialog is in page (Word/Syll) inits
      
      lpcBtnClick(); //animation located in lpc.js  

      $('.rate-btn').click(function() { 
          var rating = this.id;
          getCurrentRating(rating);  //located in lpc.js  
        });
      $("#target-btn").html("Load Target").addClass("cleared");
     
      $('#target-btn').click(function() {
          targetBtn(); //located in lpc.js 
        });

      $('#bottomDiv').css("display","none");
    } //End if
  } //End lpcLoad

  // Evaluates and clears all lpc set-ups from active page. 
    //c/b != #lpcBlock.loaded || != #lpcBlock.loaded-free
  function lpcKill() {
    //setLPCSize(170, 334, 0,0); //#ts #st #hc is this advisable?
    if ($("#lpcBlock").hasClass("loaded")) {
      $('#textbox').show().removeClass("freeRow row").addClass("rowHalf");
      $("#lpcBlock").removeClass("loaded");
      $('#target-btn').addClass('cleared');
      $("#lpcBlock").css("display","none");
      $("#midBlock").show().fadeIn(600);
      //$("#midBlock").children().show().fadeIn(600);
      $('#bottomDiv').removeClass("rowHalf").addClass("row");
     } else if ($("#lpcBlock").hasClass("loaded-free")) {
      $('#textbox').show().removeClass("freeRow row").addClass("rowHalf");
      $("#lpcBlock").removeClass("loaded-free");
      $('#target-btn').addClass('cleared');
      $("#lpcBlock").css("display","none");
      $("#midBlock").show().fadeIn(600);
      //$('#bottomDiv').removeClass("rowHalf").addClass("row");
      $('#bottomDiv').css("display","none");
     }//End if
  } //End lpcKill

  // This should only be called from inside freeInit only
  function lpcFree() {  
    setLPCSize(170, 334, 680, 310);
    $('#textbox').show().addClass("freeRow").removeClass("row rowHalf").html("This is the place to practice your /r/ sounds on your own. <br/>There are no prompts or words to match here. Its just you and the wave.");
    
    $("#midBlock").css("display","none");

    $("#lpcBlock").addClass("loaded-free").css("display","block");
    $("#lpcBlock").css("height","auto")
    $("#lpcHead, #prog, #rateBox").css("display","none");

    loadSlider();
    $('#target-btn').click(function() {
      targetBtn(); //located in lpc.js 
    });
    $('#bottomDiv').css("display","none");
  } //End lpcFree

/* ----------------------------------------------------------------------
    INIT PAGES

    NOTE: jQuery .addClass does not work with SVG. 
    To get SVG layer classes use .attr("class", ".layerName")
---------------------------------------------------------------------- */

  function profilesInit() {
    clearPage();
    //Nav Ani
      $("#profileNav").addClass("active");  // something weird is going on
      $("#profileSVG").attr("class", "animated tada");
      $("#star").attr("class", "proBlue"); 
      $("#dots").attr("class", "proWhite"); 
    
    //Page Header
      $('#pageHeader').html('PROFILES').fadeIn(600);

    // SpacerTop
    //

    //LPC or MidBlock
      lpcKill();
      $('#midBlock').show().fadeIn(600);
      $('#settings').show().appendTo('#midBlock'); //.addClass('midBlockPos');
      $('#settingsQ1 *').show();
      page1();
      //$('#settingsQ2 *').hide();
      //$('#midBlock').children().show().fadeIn(600);
      //$("#midBlock h2").html("Welcome to START!"); //.fadeIn(600);
      //$("#midBlock h3").html("");
      //$("#midBlock div").html("there");

    //Dialog
  }
  function page2() {
      $('#settingsQ1 *').hide();
      $('#settingsQ2 *').show();
  }

  function page1() {
      $('#settingsQ1 *').show();
      $('#settingsQ2 *').hide();
  }

/*
  function outputUpdate(value) {
    //$('#filterRange').value = filterOrder;
    $('#filterOutput').html(value);
    console.log(value);
  }
  */

  function outputUpdate(value) {
    //document.querySelector('#filterOutput').value = value;
	  setFilterOrder(value)
	  $('#filterOutput').html(value);
	  console.log(value);
  }

function setFilterOrder(filterOrder) {
	callObjcWithURI("lpc://setfilterorder/" + filterOrder);
}


//onchange="myFunction(this.value)"
//weight.value =  shippingweight.valueAsNumber

  function tutorialInit() {
    clearPage();
    //Nav Ani
      $("#tutNav").addClass("active");
      $("#tutSVG").attr("class", "activeSVG animated rotateIn");
      $("#negSpace").attr("class", "tutON");

      //Page Header
      $('#pageHeader').html('TUTORIAL').fadeIn(600);
      //$('#textbox').addClass("rowHalf");

      //LPC & midBlock
      lpcKill();
      $("#midBlock h2").html("Tutorial Coming Soon...").fadeIn(600);
      $("#midBlock h3").html("").fadeIn(600);
  } // END tutorialInit()


  function autoInit() {
    clearPage();
    //Nav Ani
      $("#autoNav").addClass("active");
      $("#autoSVG").attr("class", "autoON animated flipInX");

    //Page Header
      $('#pageHeader').html('AUTO SESSION').fadeIn(600);

    //LPC & midBlock
      lpcKill();
      $("#midBlock h2").html("Auto Session Coming Soon...").fadeIn(600);
      $("#midBlock h3").html("").fadeIn(600);
  } // END autoInit()  

  function freeInit() {
    clearPage();
    //Nav Ani
    $("#freeNav").addClass("active");
    $("#freeSVG").attr("class", "freeON animated bounce");

    //Page Header
    $('#pageHeader').html('FREE PLAY').fadeIn(600);

    //LPC Controls 
    if ($("#lpcBlock").hasClass("loaded")) {
      lpcKill();
      lpcFree();
    } else if (!$("#lpcBlock").hasClass("loaded")) {
        lpcFree();
    }
    $('#bottomDiv').removeClass("row").addClass("rowHalf");
    console.log('Free Play has loaded!');
  }

  function syllInit() { //#hc
    clearPage();
    
    //Nav Ani
    $("#syllableNav").addClass("active");
    $("#syllableSVG").attr("class", "animated bounceInSyll");
    $("#sylPath").attr("class", "bubs");
    
    //Page Header
    $('#pageHeader').html('SYLLABLE QUEST').fadeIn(600);
    
    //LPC
    lpcLoad();
    $("#target-btn").html("Load Target").addClass("cleared");
    $('#challengeItem').html('Coming Soon...');
    
    console.log('syllable challenge loaded!');
  }

  function wordInit() {
    clearPage();
    
    //Nav Ani
      $("#wordNav").addClass("active");
      $("#wordSVG").attr("class", "activeSVG animated bounceInWord");
      $("#wordPath").attr("class", "bubs");

    //Page Header
      $('#pageHeader').html('WORD CHALLENGE').fadeIn(600);

    //LPC & Btns
      //$('#challengeItem').html('hammer'); //#web
      lpcLoad();
      wordpractice();
      record();

    //End Session
      $('#sess-btn').click(function() {
        $('#dialogBox').show();
      });

      $('#db-close').click(function() {
        $('#dialogBox').hide();
      });

      $("#qSaveAudioY").click(function() {
        $("#qSaveAudioN").removeClass('db-btn-select');
        $(this).addClass('db-btn-select');
        console.log("qSaveAudioY");
        // need save fx #st
      });
       $("#qSaveAudioN").click(function() {
          $("#qSaveAudioY").removeClass('db-btn-select');
          $(this).addClass('db-btn-select'); 
          console.log("qSaveAudioN"); 
          // #st what happens next
        });

        $("#qUploadAudioY").click(function() {
        $("#qUploadAudioN").removeClass('db-btn-select');
        $(this).addClass('db-btn-select');
        console.log("qUploadAudioY"); 
        // #st what happens next
        });

        $("#qUploadAudioN").click(function() {
          $("#qUploadAudioY").removeClass('db-btn-select');
          $(this).addClass('db-btn-select');
          console.log("qUploadAudioN"); 
          // #st what happens next
          
        });
        $("#db-submit").click(function() {
          $('div.dialog-q button').removeClass('db-btn-select');
          $('#dialogBox').hide();
        });

      console.log('word challenge loaded!');
  }

  function resourcesInit() {
    clearPage();
    //Nav Ani
      $("#resourcesNav").addClass("active");
      $("#resourcesSVG").attr("class", "activeSVG animated flipInY");

    //Page Header
      $('#pageHeader').html('SLP RESOURCES').fadeIn(600);

    //LPC & midBlock
      lpcKill();
      $("#midBlock h2").html("Resources Library Coming Soon...").fadeIn(600);
      $("#midBlock h3").html("This section will hold reference documents for SLPs.").fadeIn(600);

      console.log('resources page has loaded!');
  } // END resourcesInit()




/* ----------------------------------------------------------------------

  ---------------------------------------------------------------------- */



  /* ALL 
  ---------------------------------------------------------------------- */
  /* PROFILES 
  ---------------------------------------------------------------------- */
  /* TUTORIAL 
  ---------------------------------------------------------------------- */
  /* AUTO 
  ---------------------------------------------------------------------- */
  /* FREE 
  ---------------------------------------------------------------------- */
  /* SYLLABLES 
  ---------------------------------------------------------------------- */
  /* WORDS
  ---------------------------------------------------------------------- */
  /* RESOURCES
  ---------------------------------------------------------------------- */




