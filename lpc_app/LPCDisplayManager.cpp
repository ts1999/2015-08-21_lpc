/**
 * @file LPCDisplayManager.cpp
 * @Author Jon Forsyth
 * @date 7/25/14
 * @brief Classes and functions used for displaying LPC magnitude spectrum and spectral peaks.
 */

#include <iostream>

#include "LPCDisplayManager.h"
#include "AudioManager.h"

#define LPC_HIST_LEN (4)        /**< number of buffers used for LPC magnitude spectrum history */
//#define LPC_NUM_DISPLAY_BINS (256)  // <-- this shouldn't need to change if sample rate is changed

#define MAX_DB_VAL (10.0)       /**< maximum level of LPC magnitude spectrum (dB) */
#define MIN_DB_VAL (-75.0)      /**< minimum level of LPC magnitude spectrum (dB) */


LPCDisplayManager::LPCDisplayManager(UInt32 numDisplayBins, Float32 sampleRate):
m_numPeaks(0)
{
    _numDisplayBins = numDisplayBins;
    m_sampleRate = sampleRate;
    
    // misc
    _displayPad = 0.1;
    
    // history buffer for smoothing out LPC magnitude computations
    _historyBuffer = new DoubleBuffer(_numDisplayBins,LPC_HIST_LEN);

    this->initOpenGL();
}

LPCDisplayManager::~LPCDisplayManager()
{
    // Delete buffers that aren't needed
    if (_vertexBufferID != 0) {
        glDeleteBuffers (1, &_vertexBufferID);
        _vertexBufferID = 0;
    }

    delete _historyBuffer;
    delete _freqVertices;
}


void LPCDisplayManager::initOpenGL()
{
    // create arrays to draw LPC mag spectrum and peaks
    _freqVertices = new SceneVertex[_numDisplayBins];
    for (int i=0; i<_numDisplayBins; i++) {
        _freqVertices[i].positionCoords.x = 0.0;
        _freqVertices[i].positionCoords.y = 0.0;
        _freqVertices[i].positionCoords.z = 0.0;
    }

    _peakVertices = new SceneVertex[2*_numDisplayBins];
    for (int i=0; i<2*_numDisplayBins; i++) {
        _peakVertices[i].positionCoords.x = 0.0;
        _peakVertices[i].positionCoords.y = 0.0;
        _peakVertices[i].positionCoords.z = 0.0;
    }
    
    // Generate, bind, and initialize contents of a buffer to be
    // stored in GPU memory
    glGenBuffers(1, &_vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, _vertexBufferID);
}

void LPCDisplayManager::render(Float32 *lpc_mag_buffer)
{
    this->render(lpc_mag_buffer,_freqVertices, _peakVertices);
}

void LPCDisplayManager::renderTargetFormantFreqs(SceneVertex *targFreqVertices, double *targFormantFreqs, int maxNumTargFormantFreqs)
{
    memset(targFreqVertices, 0, maxNumTargFormantFreqs*sizeof(SceneVertex));
    float xPos = 0;
    
    for (int i=0; i<maxNumTargFormantFreqs; i++) {
        if (targFormantFreqs[i] == 0.0) {
            continue;
        }
        
        xPos =  this->getNormalizedFreq((Float32)targFormantFreqs[i]);
        
        targFreqVertices[2*i].positionCoords.x = xPos;
        targFreqVertices[2*i].positionCoords.y = 0.0;
        targFreqVertices[2*i].positionCoords.z = 0.0;
        //        targFormantFreqs[2*i].sceneColor = GLKVector4Make(1.0f, 1.0f, 1.0f, 1.0f);
        targFreqVertices[2*i+1].positionCoords.x = xPos;
        targFreqVertices[2*i+1].positionCoords.y = 1.0;
        targFreqVertices[2*i+1].positionCoords.z = 0.0;
    }
    
}

void LPCDisplayManager::render(Float32 *lpc_mag_buffer, SceneVertex *freqVertices, SceneVertex *peakVertices)
{
    float x_pos, y_pos;
    UInt32 peakIndices[_numDisplayBins];
    memset(peakIndices, 0, _numDisplayBins * sizeof(UInt32));
    
    // find average LPC mag values
    float avgLpc[_numDisplayBins];
    memset(avgLpc, 0, sizeof(float)*_numDisplayBins);
    
    _historyBuffer->writeBuffer(lpc_mag_buffer);
    _historyBuffer->averageAllBuffers(avgLpc);
    
    // find peaks
    findMaxima(avgLpc, _numDisplayBins, &peakIndices[0], &m_numPeaks);
    
    float mag;
    int pk_cnt = 0, curr_pk_idx;
    
    float min_y_pos = -1.0;
    
    memset(freqVertices,0,_numDisplayBins * sizeof(SceneVertex));
    memset(peakVertices, 0, m_numPeaks * sizeof(SceneVertex));
    
    for (int i=0; i<_numDisplayBins; i++) {
        // scale between -1.0 and 1.0
        x_pos = (2.0*(float)i / (float)(_numDisplayBins-1)) - 1.0;
        
        mag = MAX( (float)( 20.0 * log10(fabsf(avgLpc[i])+1e-20)), MIN_DB_VAL );
        mag = MIN( mag, MAX_DB_VAL );
        
        y_pos = mag / ( MAX_DB_VAL - MIN_DB_VAL ); // + 1.0;
        
        freqVertices[i].positionCoords.x = x_pos;
        freqVertices[i].positionCoords.y = y_pos;
        freqVertices[i].positionCoords.z = 0.0;
        
        
        // This is for under-verticies:
        /*
        /////
        peakVertices[i*2].positionCoords.x = x_pos;
        peakVertices[i*2].positionCoords.y = min_mag;
        peakVertices[i*2].positionCoords.z = 0.0;
        
        //peakVertices[i*2].sceneColor = GLKVector4Make(0.625f, 0.0f, 1.0f, 1.0f);
        //peakVertices[i*2].sceneColor = GLKVector4Make(0.0f, 0.0f, 1.0f, 1.0f);
        
        peakVertices[i*2 + 1].positionCoords.x = x_pos;
        peakVertices[i*2 + 1].positionCoords.y = y_pos;
        peakVertices[i*2 + 1].positionCoords.z = 0.0;
        */
         
        //peakVertices[i*2 + 1].sceneColor = GLKVector4Make(0.0f, 0.0f, 1.0f, 1.0f);
        
        //continue;
        
        curr_pk_idx = (int)peakIndices[pk_cnt];
        if (curr_pk_idx == i) {
            peakVertices[2*pk_cnt].positionCoords.x = x_pos;
            peakVertices[2*pk_cnt].positionCoords.y = min_y_pos;
            peakVertices[2*pk_cnt].positionCoords.z = 0.0;
            peakVertices[2*pk_cnt + 1].positionCoords.x = x_pos;
            peakVertices[2*pk_cnt + 1].positionCoords.y = y_pos;
            peakVertices[2*pk_cnt + 1].positionCoords.z = 0.0;
            pk_cnt++;
        }
    }
}

Float32 LPCDisplayManager::getNormalizedFreq(Float32 freq)
/*
 * Compute the normalized frequency, so that the given
 * frequency is mapped to [0.0,1.0] (i.e. the Nyquist frequency
 * will be equal to 1.0).
 */
{
    return 2.0*freq / m_sampleRate;
}
