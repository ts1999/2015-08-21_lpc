/**
 * @file Analysis.h
 * @Author Jon Forsyth
 * @date 10/17/14.
 * @brief Classes etc. for data analysis. Currently unused.
 */


#ifndef __lpc_app__Analysis__
#define __lpc_app__Analysis__

#include <iostream>

class DoubleBuffer;

typedef struct {
    UInt32 num;
    UInt32 bin_num;
    Float32 freq_mean;
    Float32 freq_std;
    Float32 mag;
} Formant;


class LpcAnalyzer {
public:
    
    // constructor/desctructor
    LpcAnalyzer(UInt32 numTargetFormants, UInt32 sampleRate, UInt32 audioFrameSize, UInt32 numLpcBins);
    ~LpcAnalyzer();
    
    // methods
    void setTargetFormants(Formant *targets);

    void startRecord() { _isRecording = true; };
    void stopRecord() { _isRecording = false; };
    Boolean isRecording() { return _isRecording; };
    void captureLpcSpec(Float32 *lpcSpec);
    
    // helpers
    Float32 binToFreq(UInt32 binNum);
    UInt32 freqToBin(Float32 freq);
    
    // members
    UInt32 m_numTargetFormants;
    UInt32 m_sampleRate;
    UInt32 m_numLpcBins;
    UInt32 m_audioFrameSize;
    
private:
    Formant *_targetFormants;
    DoubleBuffer *_recordBuffer;
    Boolean _isRecording;
};

#endif /* defined(__lpc_app__Analysis__) */
