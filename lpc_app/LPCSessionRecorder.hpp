//
//  LPCSessionRecorder.hpp
//  lpc_app
//
//  Created by Sam Tarakajian on 12/2/15.
//
//

#ifndef LPCSessionRecorder_hpp
#define LPCSessionRecorder_hpp

#import <AudioToolbox/AudioToolbox.h>
#include "LPCRecordingSessionData.h"

NS_ASSUME_NONNULL_BEGIN

class LPCSessionRecorder {
public:
    LPCSessionRecorder(AudioStreamBasicDescription *streamASBD, AudioStreamBasicDescription *fileASBD);
    ~LPCSessionRecorder();
    
    OSStatus BeginRecording(const LPCRecordingSessionData *data);
    
    OSStatus WriteAudio(UInt32 inNumberFrames, const AudioBufferList * __nullable ioData);
    
    OSStatus WriteLPCCoefficients(const AudioTimeStamp *inTimeStamp, UInt32 inNumberCoeffs, const double * __nullable lpcCoefficients);
    
    OSStatus StopRecording();
private:
    AudioStreamBasicDescription m_streamASBD;
    AudioStreamBasicDescription m_fileASBD;
    bool m_isRecording;
    ExtAudioFileRef __nullable m_audioOutputFile;
    FILE * __nullable m_lpcOutputFile;
};

NS_ASSUME_NONNULL_END

#endif /* LPCSessionRecorder_hpp */
