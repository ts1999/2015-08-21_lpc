//
//  LPCSessionRecorder.cpp
//  lpc_app
//
//  Created by Sam Tarakajian on 12/2/15.
//
//

#include "LPCSessionRecorder.hpp"

#include <mach/mach_time.h>

static ExtAudioFileRef CreateAudioFile (CFStringRef filePath, AudioStreamBasicDescription fileASBD, AudioStreamBasicDescription streamASBD) {
    OSStatus result;
    AudioFileTypeID afTypeID = kAudioFileCAFType;
    AudioFileID	audioFileID;
    ExtAudioFileRef extDestFile;
    CFURLRef fileURL;
    fileURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault,
                                            (CFStringRef)filePath,
                                            kCFURLPOSIXPathStyle,
                                            false);
    // Create the destination audio file
    result = AudioFileCreateWithURL(fileURL,
                                    afTypeID,
                                    &fileASBD,
                                    kAudioFileFlags_EraseFile,
                                    &audioFileID);
    if (result) printf("Couldn't Create File: %d", (int)result);
        // Wrap file extAudioFileObject
    result = ExtAudioFileWrapAudioFileID(audioFileID, true, &extDestFile);
    if (result) printf("Couldn't Wrap File: %d", (int)result);
        // Set file stream description
    result = ExtAudioFileSetProperty(extDestFile,
                                     kExtAudioFileProperty_ClientDataFormat,
                                     sizeof(AudioStreamBasicDescription),
                                     &streamASBD);
    if (result) printf("Couldn't Set File Stream Format: %d %u", (int)result, (unsigned int)streamASBD.mChannelsPerFrame );
        // pre-write nil to prevent allocation pauses in the render callback
    result = ExtAudioFileWriteAsync(extDestFile, 0, NULL);
    if (result) printf("Couldn't Pre Write File: %d", (int)result);
                    
    return extDestFile;
}

LPCSessionRecorder::LPCSessionRecorder(AudioStreamBasicDescription *streamASBD, AudioStreamBasicDescription *fileASBD)
{
    m_streamASBD = *streamASBD;
    m_fileASBD = *fileASBD;
}

LPCSessionRecorder::~LPCSessionRecorder() {}

OSStatus LPCSessionRecorder::BeginRecording(const LPCRecordingSessionData *data)
{
    FILE *metadataFile = fopen(data->metadata_path, "w");
    if (!metadataFile) {
        printf("LPCSessionRecorder: Could not open metadata file at path %s\n", data->metadata_path);
        return -50;
    }
    
    const char *metadataHeader = "stream_sample_rate, uuid, deviceID, username, gender, age, heightFeet, heightInches, start_date, lpc_order\n";
    fwrite(metadataHeader, sizeof(char), strlen(metadataHeader)+1, metadataFile);
    fprintf(metadataFile,
            "%f, %s, %s, %s, %s, %d, %d, %d, %s, %d",
            m_streamASBD.mSampleRate,
            data->accountUUID,
            data->identifier,
            data->username,
            data->gender,
            data->ageInYears,
            data->heightFeet,
            data->heightInches,
            data->date_string,
            data->lpc_order);
    fclose(metadataFile);
    
    m_lpcOutputFile = fopen(data->lpc_path, "w");
    if (!m_lpcOutputFile) {
        printf("LPCSessionRecorder: Could not open LPC file at path %s\n", data->lpc_path);
        return -50;
    }
    fprintf(m_lpcOutputFile, "sample_time");
    for (int i=0; i<data->lpc_order; ++i) {
        fprintf(m_lpcOutputFile, ",lpc_%d", i);
    }
    fprintf(m_lpcOutputFile, "\n");
    
    CFStringRef pathRef = CFStringCreateWithCStringNoCopy(NULL, data->audio_path, kCFStringEncodingMacRoman, kCFAllocatorNull);
    m_audioOutputFile = CreateAudioFile(pathRef, m_fileASBD, m_streamASBD);
    CFRelease(pathRef);
    if (!m_audioOutputFile) {
        printf("LPCSessionRecorder: Could not open audio file at path %s\n", data->audio_path);
        return -50;
    }
    
    m_isRecording = true;
    
    return 0;
}

OSStatus LPCSessionRecorder::WriteAudio(UInt32					inNumberFrames,
                                        const AudioBufferList   * __nullable ioData)
{
    return ExtAudioFileWriteAsync(m_audioOutputFile, inNumberFrames, ioData);
}

OSStatus LPCSessionRecorder::WriteLPCCoefficients(const AudioTimeStamp  *inTimeStamp,
                                                  UInt32                inNumberCoeffs,
                                                  const double          *lpcCoefficients)
{
    static mach_timebase_info_data_t timeBaseInfo;
    static double time2nsFactor;
    if (timeBaseInfo.denom == 0) {
        (void) mach_timebase_info(&timeBaseInfo);
        time2nsFactor = (double) timeBaseInfo.numer / timeBaseInfo.denom;
        time2nsFactor /= pow(10, 9);
    }
    double timeStamp = (inTimeStamp->mHostTime * time2nsFactor);
    
    fprintf(m_lpcOutputFile, "%f,", timeStamp);
    for (UInt32 i=0; i<inNumberCoeffs; ++i) {
        if (i==inNumberCoeffs-1)
            fprintf(m_lpcOutputFile, "%f\n", lpcCoefficients[i]);
        else
            fprintf(m_lpcOutputFile, "%f,", lpcCoefficients[i]);
    }
    
    return 0;
}

OSStatus LPCSessionRecorder::StopRecording()
{
    m_isRecording = false;
    AudioFileID audioFileID;
    UInt32 dataSize = sizeof(AudioFileID);
    ExtAudioFileGetProperty(m_audioOutputFile,
                            kExtAudioFileProperty_AudioFile,
                            &dataSize,
                            &audioFileID);
    AudioFileClose(audioFileID);
    fclose(m_lpcOutputFile);
    return ExtAudioFileDispose(m_audioOutputFile);
}