/**
 * @file Analysis.cpp
 * @Author Jon Forsyth
 * @date 10/17/14.
 * @brief Classes etc. for data analysis. Currently unused.
 */


#include "Analysis.h"
#include "AudioManager.h"

#define MAX_RECORD_TIME_SEC (2.0)

LpcAnalyzer::LpcAnalyzer(UInt32 numTargetFormants, UInt32 sampleRate, UInt32 audioFrameSize, UInt32 numLpcBins)
{

    m_numTargetFormants = numTargetFormants;
    m_sampleRate = sampleRate;
    m_numLpcBins = numLpcBins;
    m_audioFrameSize = audioFrameSize;
    
    _isRecording = false;
    
    _targetFormants = new Formant[m_numTargetFormants];
    
    int nRecFrames = ceil(MAX_RECORD_TIME_SEC * ((float)m_sampleRate / (float)m_audioFrameSize));
    _recordBuffer = new DoubleBuffer(m_numLpcBins,nRecFrames);
    
}

LpcAnalyzer::~LpcAnalyzer()
{
    delete _targetFormants;
    delete _recordBuffer;
}

void LpcAnalyzer::captureLpcSpec(Float32 *lpcSpec)
{
    if (_isRecording) {
        _recordBuffer->writeBuffer(lpcSpec);
    }
}


void LpcAnalyzer::setTargetFormants(Formant *targets)
{
    for (int i=0; i<m_numTargetFormants; i++) {
        _targetFormants[i].freq_mean = targets[i].freq_mean;
        _targetFormants[i].freq_std = targets[i].freq_std;
        _targetFormants[i].num = targets[i].num;
        _targetFormants[i].bin_num = this->freqToBin(targets[i].freq_mean);
        _targetFormants[i].mag = targets[i].mag;
    }
}


Float32 LpcAnalyzer::binToFreq(UInt32 binNum)
{
    return (Float32)(binNum * m_sampleRate) / (Float32)( 2.0*(m_numLpcBins-1));
}


UInt32 LpcAnalyzer::freqToBin(Float32 freq)
{
    return (2.0 * freq * (m_numLpcBins-1)) / (Float32)m_sampleRate;
}